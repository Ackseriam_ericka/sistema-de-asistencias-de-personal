#!/bin/bash
#variables
DIA_ACTUAL=`date +%a`
FECHA_ACTUAL=`date +%Y-%m-%d`
HORA_ACTUAL=`date +%H-%M-%S`
#contraseña de postgresql
export PGPASSWORD='sicap'
export PGUSER='sicap'
#nombre del archivo
ARCH_RESP="$FECHA_ACTUAL-$HORA_ACTUAL"
#directorio
DIR_RESP='/var/www/sicap/RespaldosDB'
#bases de datos
DB_ARRAY=('sicap')
#respaldo
index=0
count=”${#DB_ARRAY[*]}”
echo ‘Respaldando la Base de Datos de CHAP’
#while [ $index -lt $count ];
#do
#dbname=”${#DB_ARRAY[$index]}”
echo 'respaldando: ' ${DB_ARRAY[$index]-i}
#pg_dump -h localhost -F c -b -f $DIR_RESP/${DB_ARRAY[$index]}/${DB_ARRAY[$index]}-$ARCH_RESP.backup ${DB_ARRAY[$index] -i }
pg_dump -h localhost -F p -a --column-inserts -b -f $DIR_RESP/chap-$ARCH_RESP.sql ${DB_ARRAY[$index] -i }
##i=$((i+1))
#let “index = $index + 1”
echo 'Listo'
#
unset PGUSER
unset PGPASSWORD

