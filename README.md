# Sistema de Control Horarios y Asistencias de Personal (CHAP) #

Este es el repositorio donde se alojara el desarrollo del Sistema de Control de Horarios y Asistencias de Personal para Mukumbarí, Sistema Teleférico de Mérida, como parte del proyecto socio-tecnológico de lo desarrolladores en la [Universidad Politécnica Territorial del Estado Mérida "Kleber Ramírez"](http://uptm.edu.ve)

## Desarrolladores ##

* Jesús Vielma
* Ericka Simancas
* Joeinny Osorio
* Fernando Araque

## Tutores y Guias ##

* Rodolfo Gonzalez: Tutor académico y docente guía de la UPTMKR.
* Mauricio Irady: Coordinador de Sistemas y Tecnologías de la Información de Mukumbari Sistema Teleférico de Mérida.
* Andreina Briceño: Técnico de Talento Humano de Mukumbari Sistema Teleférico de Mérida.

## Requisitos para la instalación ##
* Apache2 
* PHP5 
* PostgreSQL 9.0 o superior 
* Demonio CRON (Sistemas UNIX)

## Adicional ##
* Configurar Apache para que acepte archivos .htaccess

## Instalación de CHAP ##
1. Descargue los archivos desde [este enlace](https://bitbucket.org/jesus_vielma/sistema-de-asistencias-de-personal/get/master.zip) 
2. Descomprima el archivo descargado dentro del directorio correspondiente a los archivos de apache (en sistema basados en Debian este se encuentra ubicado en /var/www/) y renombre la carpeta con la palabra sicap
3. Ubique el archivo config.php que se encuentra dentro de la carpeta applications/config y proceda a editar la variable $config['base_url']	= 'http://localhost/sicap/'; cambie la dirección URL que allí aparece por la URL que usara para acceder al sistema.
4. Dentro del archivo config.php ubique la variable $config['index_page'] = 'index.php'; borre el contenido que este dentro de las comillas simples. 
5. Guarde sus cambios y verifique los cambios realizados.
6. Un vez el sistema esta instalado deberá crear un usuario para la base de datos del sistema. Este usuario deberá llamarse sicap y con clave sicap. 
7. Una vez creado el usuario para la base de datos proceda a crear una nueva base de datos con nombre sicap, cuyo dueño u owner sera el usuario sicap.
8. Ya creada la base datos proceda a ejecutar los scripts sql en el siguiente orden (dichos archivos se encuentran dentro de la carpeta db/), estos archivos contienen la estructura de la base de datos, y la información básica para el uso del sistema. sicap_postgresql.sql, sicap_base.sql, empl.sql, fecha_nac-STM.sql, administrativo.sql, operativo.sql. Los primeros 4 scripts ejecutaran la creación de los departamentos, empleados, cargos y nomina de la empresa. Los ultimos dos scripts ejecutaran la creación de asignaciones de horarios para personal Adminsitrativo y Operativo, debido a que el resto de horarios son cambiantes se deberan hacer dichas asignaciones de horarios a los empelados con la interfaz del sistema.
9. CUando toda la data este cargada podra acceder al sistema, con el usuario admin y la clave admin (Este usuario ya este definido)

## Uso del sistema ##
EL sistema esta diseñado para ser completamente funcional en los navegadores: 

* Google Chrome 44 o posterior.
* Firefox 40 o superior