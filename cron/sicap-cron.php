<?php
date_default_timezone_set('America/Caracas');
setlocale(LC_ALL,'es_VE.UTF-8','es_VE','Windows-1252','esp','es_ES.UTF-8','es_ES');
$conexion=pg_connect("host=localhost port=5432 dbname=sicap user=sicap password=sicap") or
  die("Problemas en la conexion");
/*mysql_select_db("sicap",$conexion)or
  die("Problemas en la selección de la base de datos")*/;
$registros=pg_query($conexion,"select * from empleado where estatus='Activo';") /*or
  die("Problemas en el select:".mysql_error())*/;
  
$numero = pg_num_rows($registros);

pg_close($conexion);
$time = date("H:i:s");
$date = date("d-m-Y");

//$as='';
$hechas =0;
while($row = pg_fetch_array($registros))
{
	$id = $row['empleado_id'];
	
	$conexion=pg_connect("host=localhost port=5432 dbname=sicap user=sicap password=sicap") or
  die("Problemas en la conexion");
  
  	$asignaciones=pg_query($conexion,"select asignacion.*, horario.* from asignacion
						   INNER JOIN horario on asignacion.horario_id = horario.horario_id
						   where empleado_id='$id' order by fecha desc limit 1;") or
		die("Problemas 1");
	
	
	//$as.="El empleado ".$row['nombre']." tiene un total de ".pg_numrows($asignaciones)." asignaciones <br>";
	/*while($fi =pg_fetch_array($asignaciones))
	{
		$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']." Horario: ".$fi['tipo']."<br>";
	}*/
				$fecha = date('Y-m-d');
				$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
				$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
				$dia = ucfirst(strftime('%A',strtotime($nuevafecha)));
pg_query("BEGIN") or die("No se puede inicar la transaccion\n");
	while($fi = pg_fetch_array($asignaciones))
	{
		//print_r($fi);
		
		//Asignacion para los empleados que trabajan en horario tipo Normal
		if($fi['tipo']=='Normal')
		{
			if($fi['dias']=='X a D')
			{
				/*$as.="Horario Normal entrada 7am<br>";
				$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']."<br>";*/
				
				if($fi['dia']=='Domingo' || $fi['dia']=='Lunes')
				{
					$empl_id = $row['empleado_id'];
					$horario = $fi['horario_id'];
					$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							 VALUES ('$nuevafecha','No','No','$dia','Libre','$empl_id','$horario');");
					$hechas = $hechas +1;
				}
				else
				{
					$empl_id = $row['empleado_id'];
					$horario = $fi['horario_id'];
					$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							 VALUES ('$nuevafecha','No','Si','$dia','Día','$empl_id','$horario');");
					$hechas = $hechas +1;
				}
			}
			elseif($fi['dias']=='L a V')
			{
				/*$as.="Horario Normal entrada 8am<br>";
				$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']."<br>";*/
				if($fi['dia']=='Sábado' || $fi['dia']=='Viernes')
				{
					
					$empl_id = $row['empleado_id'];
					$horario = $fi['horario_id'];
					$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							 VALUES ('$nuevafecha','No','No','$dia','Libre','$empl_id','$horario');");
					$hechas = $hechas +1;
				}
				else
				{
					$empl_id = $row['empleado_id'];
					$horario = $fi['horario_id'];
					$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							 VALUES ('$nuevafecha','No','Si','$dia','Día','$empl_id','$horario');");
					$hechas = $hechas +1;
				}
			}
			
			else
			{//Personal de estacion 
				/*$as.="Horario Normal entrada 8am<br>";
				$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']."<br>";*/
				if($fi['dia']=='Domingo' || $fi['dia']=='Lunes')
				{
					
					$empl_id = $row['empleado_id'];
					$horario = 11;
					$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							 VALUES ('$nuevafecha','No','No','$dia','Libre','$empl_id','$horario');");
					$hechas = $hechas +1;
				}
				elseif($fi['dia']=='Viernes' || $fi['dia']=='Sábado')
				{
					$empl_id = $row['empleado_id'];
					$horario = 14;
					$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							 VALUES ('$nuevafecha','No','Si','$dia','Día','$empl_id','$horario');");
					$hechas = $hechas +1;
				}
				elseif($fi['dia']=='Miercoles')
				{
					$empl_id = $row['empleado_id'];
					$horario = 12;
					$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							 VALUES ('$nuevafecha','No','Si','$dia','Día','$empl_id','$horario');");
					$hechas = $hechas +1;
				}
				elseif($fi['dia']=='Jueves')
				{
					$empl_id = $row['empleado_id'];
					$horario = 13;
					$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							 VALUES ('$nuevafecha','No','Si','$dia','Día','$empl_id','$horario');");
					$hechas = $hechas +1;
				}
			}
		}
		
		//Asignacion para el tipo de horario 12x12
		elseif($fi['tipo']=='12x12')
		{
			if($fi['fecha']==date('Y-m-d')) //Busca que la ultima fecha de la asignación sea igual al fecha actual para poder realizar la asignacion de los 12x12
			{
				$nf = strtotime ( '-1 day' , strtotime ( $fi['fecha'] ) ) ;
				$nf = date ( 'Y-m-d' , $nf );
				
				$asignaciones1=pg_query($conexion,"select asignacion.*, horario.* from asignacion
							   INNER JOIN horario on asignacion.horario_id = horario.horario_id
							   where empleado_id='$id' and fecha='$nf' order by fecha desc limit 1;") or
				die("Problemas en el select 2.");
				
				while($fi1=pg_fetch_array($asignaciones1))
				{
					$nuevafecha1 = strtotime ( '+1 day' , strtotime ( $nuevafecha ) ) ;
					$nuevafecha1 = date ( 'Y-m-d' , $nuevafecha1 );
					$dia2 = ucfirst(strftime('%A',strtotime($nuevafecha1)));
					
					//print_r($fi1);
					if($fi['turno']=='Día' && $fi1['turno']=='Día')
					{
						/*$as.="Horario 12x12 entrada 7am<br>";
						$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']."<br>";
						$as.="Empleado: ".$row['nombre']." Fecha: ".$fi1['fecha']." Turno: ".$fi1['turno']."<br>";*/
						$empl_id = $row['empleado_id'];
						$horario = $fi['horario_id'];
						//$as.="Mañana es: ".$dia." ".$nuevafecha." Pasado Mañana es: ".$dia2." ".$nuevafecha1." Horario_id = ".$horario."<br>"; 
						$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							      VALUES ('$nuevafecha','No','Si','$dia','Libre','$empl_id','$horario'),
							             ('$nuevafecha1','No','Si','$dia2','Libre','$empl_id','$horario');");
						$hechas = $hechas +1;
					}
					elseif($fi['turno']=='Noche' && $fi1['turno']=='Noche')
					{
						/*$as.="Horario Normal entrada 7pm<br>";
						$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']."<br>";
						$as.="Empleado: ".$row['nombre']." Fecha: ".$fi1['fecha']." Turno: ".$fi1['turno']."<br>";*/
						$empl_id = $row['empleado_id'];
						$horario = $fi['horario_id'];
						//$as.="Mañana es: ".$dia." ".$nuevafecha." Pasado Mañana es: ".$dia2." ".$nuevafecha1." Horario_id = ".$horario."<br>"; 
						$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							      VALUES ('$nuevafecha','No','Si','$dia','Libre','$empl_id','$horario'),
							             ('$nuevafecha1','No','Si','$dia2','Libre','$empl_id','$horario');");
						$hechas = $hechas +1;
					}
					elseif($fi['turno']=='Libre' && $fi1['turno']=='Libre' && $fi['hora_entrada']=='07:00:00')
					{
						
						/*$as.="Horario Normal entrada 7am dia libre<br>";
						$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']."<br>";
						$as.="Empleado: ".$row['nombre']." Fecha: ".$fi1['fecha']." Turno: ".$fi1['turno']."<br>";*/
						
						$empl_id = $row['empleado_id'];
						$horario = 6;
					
						$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							      VALUES ('$nuevafecha','No','Si','$dia','Noche','$empl_id','$horario'),
							             ('$nuevafecha1','No','Si','$dia2','Noche','$empl_id','$horario');");
						$hechas = $hechas +1;
					}
					else
					{
						/*$as.="Horario Normal entrada 7pm dia libre<br>";
						$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']."<br>";
						$as.="Empleado: ".$row['nombre']." Fecha: ".$fi1['fecha']." Turno: ".$fi1['turno']."<br>";*/
						
						$empl_id = $row['empleado_id'];
						$horario = 5;
						
						$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							      VALUES ('$nuevafecha','No','Si','$dia','Día','$empl_id','$horario'),
							             ('$nuevafecha1','No','Si','$dia2','Día','$empl_id','$horario');");
						$hechas = $hechas +1;
					}
				}
			}
		}
		elseif($fi['tipo']=='Pernocta')
		{
			if($fi['fecha']==date('Y-m-d'))
			{
				$nf = strtotime ( '-1 day' , strtotime ( $fi['fecha'] ) ) ;
				$nf = date ( 'Y-m-d' , $nf );
				
				$asignaciones1=pg_query($conexion,"select asignacion.*, horario.* from asignacion
							   INNER JOIN horario on asignacion.horario_id = horario.horario_id
							   where empleado_id='$id' and fecha='$nf' order by fecha desc limit 1;") or
				die("Problemas en el select 2.");
				
				while($fi1=pg_fetch_array($asignaciones1))
				{
					$nuevafecha1 = strtotime ( '+1 day' , strtotime ( $nuevafecha ) ) ;
					$nuevafecha1 = date ( 'Y-m-d' , $nuevafecha1 );
					$dia2 = ucfirst(strftime('%A',strtotime($nuevafecha1)));
					
					$nuevafecha2 = strtotime ( '+1 day' , strtotime ( $nuevafecha1 ) ) ;
					$nuevafecha2 = date ( 'Y-m-d' , $nuevafecha2 );
					$dia3 = ucfirst(strftime('%A',strtotime($nuevafecha2)));
					
					if($fi['pernocta']=='Si' && $fi1['pernocta']=='Si')
					{
						$empl_id = $row['empleado_id'];
						$horario = $fi['horario_id'];
						
						$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
							      VALUES ('$nuevafecha','No','No','$dia','Libre','$empl_id','$horario'),
							             ('$nuevafecha1','No','No','$dia2','Libre','$empl_id','$horario'),
										 ('$nuevafecha2','No','No','$dia3','Libre','$empl_id','$horario');");
						$hechas = $hechas +1;
					}
					else
					{
						if($fi['turno']=='Libre' && $fi1['turno']=='Día')
						{
							$empl_id = $row['empleado_id'];
							$horario = 7;
							
							$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
									  VALUES ('$nuevafecha','Si','Si','$dia','Día','$empl_id','$horario'),
											 ('$nuevafecha1','Si','Si','$dia2','Día','$empl_id','$horario');");
							$hechas = $hechas +1;
						}
						elseif($fi['turno']=='Libre' && $fi1['turno']=='Libre')
						{
							$empl_id = $row['empleado_id'];
							$horario = 8;
							
							$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
									  VALUES ('$nuevafecha','No','Si','$dia','Día','$empl_id','$horario'),
											 ('$nuevafecha1','No','Si','$dia2','Día','$empl_id','$horario');");
							$hechas = $hechas +1;
						}
						elseif($fi['turno']=='Día' && $fi1['turno']=='Día')
						{
							$empl_id = $row['empleado_id'];
							$horario = 8;
							
							$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
									  VALUES ('$nuevafecha','No','No','$dia','Libre','$empl_id','$horario'");
							$hechas = $hechas +1;
						}
					}
				}
			}
			
		}
		elseif($fi['tipo']=='Otros')
		{
			if($fi['fecha']==date('Y-m-d'))
			{
				$nf = strtotime ( '-1 day' , strtotime ( $fi['fecha'] ) ) ;
				$nf = date ( 'Y-m-d' , $nf );
				
				$asignaciones1=pg_query($conexion,"select asignacion.*, horario.* from asignacion
							   INNER JOIN horario on asignacion.horario_id = horario.horario_id
							   where empleado_id='$id' and fecha='$nf' order by fecha desc limit 1;") or
				die("Problemas en el select 2.");
				
				$nf1 = strtotime ( '-1 day' , strtotime ( $nf ) ) ;
				$nf1 = date ( 'Y-m-d' , $nf1 );
				
				$asignaciones2=pg_query($conexion,"select asignacion.*, horario.* from asignacion
							   INNER JOIN horario on asignacion.horario_id = horario.horario_id
							   where empleado_id='$id' and fecha='$nf1' order by fecha desc limit 1;") or
				die("Problemas en el select 2.");
				
				$nf2 = strtotime ( '-1 day' , strtotime ( $nf1 ) ) ;
				$nf2 = date ( 'Y-m-d' , $nf2 );
				
				$asignaciones3=pg_query($conexion,"select asignacion.*, horario.* from asignacion
							   INNER JOIN horario on asignacion.horario_id = horario.horario_id
							   where empleado_id='$id' and fecha='$nf2' order by fecha desc limit 1;") or
				die("Problemas en el select 2.");
				
				if($fi['hora_entrada']=='07:30:00')
				{
					$nuevafecha1 = strtotime ( '+1 day' , strtotime ( $nuevafecha ) ) ;
					$nuevafecha1 = date ( 'Y-m-d' , $nuevafecha1 );
					$dia2 = ucfirst(strftime('%A',strtotime($nuevafecha1)));
					
					while($fi1=pg_fetch_array($asignaciones1))
					{
						while($fi2=pg_fetch_array($asignaciones2))
						{
							while($fi3=pg_fetch_array($asignaciones3))
							{
								if(($fi['turno']=='Libre' && $fi1['turno']=='Libre' && $fi2['turno']=='Libre' && $fi3['turno']=='Libre') || ($fi['turno']=='Día' && $fi1['turno']=='Día' && $fi2['turno']=='Libre' && $fi3['turno']=='Libre'))
								{
									$empl_id = $row['empleado_id'];
									$horario = $fi['horario_id'];
									
									$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
											  VALUES ('$nuevafecha','No','Si','$dia','Día','$empl_id','$horario'),
													 ('$nuevafecha1','No','Si','$dia2','Día','$empl_id','$horario');");
									$hechas = $hechas +1;
								}
								elseif(($fi['turno']=='Día' && $fi1['turno']=='Día' && $fi2['turno']=='Día' && $fi3['turno']=='Día') || ($fi['turno']=='Libre' && $fi1['turno']=='Libre' && $fi2['turno']=='Día' && $fi3['turno']=='Día'))
								{
									$empl_id = $row['empleado_id'];
									$horario = $fi['horario_id'];
									
									$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
											  VALUES ('$nuevafecha','No','No','$dia','Libre','$empl_id','$horario'),
													 ('$nuevafecha1','No','No','$dia2','Libre','$empl_id','$horario');");
									$hechas = $hechas +1;
								}
							}
						}
					}
				}
				elseif($fi['hora_entrada']=='14:00:00')
				{
					if($fi['dia']=='Sábado' || $fi['dia']=='Viernes')
					{
						
						$empl_id = $row['empleado_id'];
						$horario = $fi['horario_id'];
						$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
								 VALUES ('$nuevafecha','No','No','$dia','Libre','$empl_id','$horario');");
						$hechas = $hechas +1;
					}
					else
					{
						$empl_id = $row['empleado_id'];
						$horario = $fi['horario_id'];
						$insert = pg_query("INSERT INTO asignacion (fecha,pernocta,trabajo,dia,turno,empleado_id,horario_id)
								 VALUES ('$nuevafecha','No','Si','$dia','Día','$empl_id','$horario');");
						$hechas = $hechas +1;
					}
				}
			}
			
		}
		/*else
		{
			if($fi['tipo']=='Pernocta')
			{
				$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']."<br>";
			}
			else
			{
				$as.="Empleado: ".$row['nombre']." Fecha: ".$fi['fecha']." Turno: ".$fi['turno']."<br>";
			}
		}*/
	}
	
	
	//Para asegurar la asignacion a todos los empleados
	if ($insert)
	  pg_query("COMMIT") or die("Fallo al insertar los datos \n");
	  else
	  pg_query("ROLLBACK") or die("Deshaciendo los cambios \n");
	pg_close($conexion);
	
}

//print_r($entry);

$entry = "El $date a las $time habian $numero empleados activos. \n";
$entry.= "Se registraron asignaciones para $hechas empleados. \n";
$entry.= "---------------------------------------------------------\n\n";

$file = "/var/www/sicap/cron/sicap-cron.txt";
$open = fopen($file,"a");

if ( $open ) {
	fwrite($open,$entry);
	fclose($open);
}

//print_r($entry);
?>
