<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Feriados_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function insertar_feriados($_data){
        $data = array(
            
            'dia'            => $_data->post('dia'),
            'mes'       => $_data->post('mes'),
            'ano'       => $_data->post('ano'),
            'descripcion'       => $_data->post('descripcion')
    
        );
        if($this->db->insert('feriados',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function obtener_todos(){
        $query = $this->db->get('feriados');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_mes_ano($mes, $ano){
        $where = "mes = '$mes' AND ano='$ano' OR mes='$mes' AND ano='0'";
        $query = $this->db->select('dia')
                          ->where($where)
                          ->order_by('dia asc')
                          ->get('feriados');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_id($_id){
        $query = $this->db->where('feriado_id',$_id)->get('feriados');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function fijos($ano){
        $sql= "select * from feriados where ano = '$ano'";
        $result = $this->db->query($sql);
        
        return $result;
    }
    
    function movibles(){
        $sql= "select * from feriados where ano <> '0'";
        $result = $this->db->query($sql);
        
        return $result;
    }
    
    function actualizar_feriados($_data){
        
        $data = array(
            'dia'            => $_data->post('dia'),
            'mes'       => $_data->post('mes'),
            'ano'       => $_data->post('ano'),
            'descripcion'       => $_data->post('descripcion'),
        );
        
        if($this->db->where('feriado_id',$_data->post('feriado_id'))->update('feriados',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function borrar_feriados($id){
        
        if($this->db->where('feriado_id',$id)->delete('feriados')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
   }
//editado
