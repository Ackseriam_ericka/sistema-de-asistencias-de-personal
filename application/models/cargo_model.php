<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class   Cargo_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function insertar_cargo($_data){
        $data = array(
            'cargo'              => $_data->post('cargo'), //aqui va igual a los nombre de bd
            'descripcion'        => $_data->post('descripcion')
            
        );
        if($this->db->insert('cargo_empl',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function obtener_todos(){
        $query = $this->db->order_by('cargo_id ASC')
                          ->get('cargo_empl');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_id($_id){
        $query = $this->db->where('cargo_id',$_id)->get('cargo_empl');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    
    function actualizar_cargo($_data){
        
        $data = array(
            'cargo'              => $_data->post('cargo'), //aqui va igual a los nombre de bd
            'descripcion'        => $_data->post('descripcion')
        );
        
        if($this->db->where('cargo_id',$_data->post('cargo_id'))->update('cargo_empl',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function borrar_cargo($id){
        
        if($this->db->where('cargo_id',$id)->delete('cargo_empl')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}	
