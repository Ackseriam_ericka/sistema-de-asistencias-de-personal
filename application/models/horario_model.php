<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Horario_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function insertar_horario($_data){
        $data = array(
            'hora_entrada'      => $_data->post('hora_entrada'),
            'hora_salida'       => $_data->post('hora_salida'),
            'descripcion'       => $_data->post('descripcion'),
            'tipo'              => $_data->post('tipo'),
            'dias'              => $_data->post('dias'),
            'nombre'            => $_data->post('nombre')
        );
        if($this->db->insert('horario',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function obtener_todos(){
        $query = $this->db->get('horario');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_id($_id){
        $query = $this->db->where('horario_id',$_id)->get('horario');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function lastid(){
        $query = $this->db->select('MAX(horario_id) AS id')
                          ->get('horario');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function actualizar_horario($_data){
        
        $data = array(
            'hora_entrada'      => $_data->post('hora_entrada'),
            'hora_salida'       => $_data->post('hora_salida'),
            'descripcion'       => $_data->post('descripcion'),
            'tipo'              => $_data->post('tipo'),
            'dias'              => $_data->post('dias'),
            'nombre'            => $_data->post('nombre')
        );
        
        if($this->db->where('horario_id',$_data->post('horario_id'))->update('horario',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function borrar_horario($id){
        
        if($this->db->where('horario_id',$id)->delete('horario')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}	
