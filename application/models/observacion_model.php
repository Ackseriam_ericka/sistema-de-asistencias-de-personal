<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Observacion_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function insertar_observacion($_data){
        $data = array(
             
            'descripcion'      => $_data->post('descripcion'), //aqui va igual a los nombre de bd
            'tipo'             => $_data->post('tipo'),
            'asistencia'        => $_data->post('asistencia'),
            'empleado_id'       => $_data->post('empleado_id'),
            'fecha'             => $_data->post('fecha'),
            'nombre_supervisor' => $_data->post('usuario'),
            
        );
      
        if($this->db->insert('justificacion',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function obtener_todos()
    {
        $query = $this->db->select ('justificacion.codigo_justi, justificacion.fecha as fecha, justificacion.descripcion as descripcion, justificacion.tipo as tipo, justificacion.asistencia as  asistencia,
                            empleado.nombre as nombre, empleado.cedula as cedula')
                          ->join ('empleado', 'justificacion.empleado_id = empleado.empleado_id')
                          ->get('justificacion');
                    
                    if($query->num_rows() > 0){
                        foreach($query->result() as $row){
                            $data[] = $row;
                        }
                        return $data;
                    }
                    else{
                        return 0;
                    }
    }
    
    function obtener_por_id($_id){
        $query = $this->db->where('codigo_justi',$_id)->get('justificacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_empleado_id($_id){
        $query = $this->db->where('empleado_id',$_id)->get('justificacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function lastid(){
        $query = $this->db->select('MAX(codigo_justi) AS id')
                          ->get('justificacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

     function obtener_justificacion($_id){
        $query = $this->db->select('justificacion.codigo_justi as codigo_justi, justificacion.descripcion as descripcion, justificacion.tipo as tipo, justificacion.asistencia as  asistencia,
                                     empleado.nombre as nombre, empleado.cedula as cedula, justificacion.fecha as fecha, justificacion.nombre_supervisor')
                          ->join ('empleado','justificacion.empleado_id= empleado.empleado_id')
                           ->where('codigo_justi = ',$_id)
                          ->get('justificacion');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function actualizar_observacion($_data){
        
        $data = array(

            'descripcion'        => $_data->post('descripcion'),
            'tipo'               => $_data->post('tipo'),
            'asistencia'         => $_data->post('asistencia'),
            'empleado_id'        => $_data->post('empleado_id')
        );
        
        if($this->db->where('codigo_justi',$_data->post('codigo_justi'))->update('justificacion',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function borrar_observacion($id){
        
        if($this->db->where('codigo_justi',$id)->delete('justificacion')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}   
