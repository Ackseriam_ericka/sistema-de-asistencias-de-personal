<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Marcaje_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
        
    function obtener_todos(){
        $query = $this->db->select('es.es_id, es.fecha as fecha, es.hora as hora, es.tipo, es.tiempo_extra, es.tiempo_extra_noche, es.dia, es.empleado_id as empleado_id, 
                                  empleado.nombre as nombre, empleado.cedula as cedula')
                          ->join ('empleado', 'es.empleado_id = empleado.empleado_id')
                          ->where('es.tipo = ', 'Entrada')
                          ->get('es');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    function obtener_todos_noti(){
        $query = $this->db->select('fallas_empl.falla_id, fallas_empl.empleado_id AS empleado_id, fallas_empl.hora as hora, fallas_empl.fecha as fecha,   fallas_empl.dia as dia,
                                    fallas_empl.tipo as tipo,  fallas_empl.descripcion, empleado.departamento_id as departamento_id,
                                    departamento.nombre AS dep_nombre, empleado.nombre as nombre')
                          ->join('empleado','fallas_empl.empleado_id = empleado.empleado_id','INNER')
                          ->join('departamento','empleado.departamento_id = departamento.departamento_id','INNER')
                          ->get('fallas_empl');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
  
   function insertar_marcaje($_data){
        $data = array(
      
            'fecha'         => $_data->post('fecha'),
            'hora'            => $_data->post('hora'),
            'dia'            => $_data->post('dia'),
            'tipo'           => $_data->post('tipo'),
            'tiempo_extra'     => $_data->post('horaex'),
           'tiempo_extra_noche'     => $_data->post('horaex_n'),
           'bono_noc'     => $_data->post('bono_noc'),
            'empleado_id'       => $_data->post('id'),
        );
        if($this->db->insert('es',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
   function insertar_notificacion($_data){

        $data = array(
      
            'fecha'         => $_data->post('fecha'),
            'hora'            => $_data->post('hora'),
            'dia'            => $_data->post('dia'),
            'tipo'           => $_data->post('tipo'),
            'descripcion'           => $_data->post('descripcion'),
            'empleado_id'       => $_data->post('id'),
            'status'            => $_data->post('status'),
            'status_supervisor'            => $_data->post('status_supervisor')
        
        );
        if($this->db->insert('fallas_empl',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


   function obtener_por_codigoemple($_id) //uso
    {
      
        $query = $this->db->select  ('departamento.departamento_id, departamento.nombre as nombre_departamento, empleado.empleado_id AS empleado_id, empleado.nombre as nombre, empleado.cedula as cedula,  empleado.codigo_empl as codigo_empl')
                          ->join ('departamento','empleado.departamento_id=departamento.departamento_id')
                          ->where('codigo_empl = ', $_id)
                          ->get('empleado');

     
      if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    function obtener_dosultimas_asig($id)   
     {
       
         $query = $this->db->select ('asignacion.asignacion_id, asignacion.fecha,asignacion.horario_id, asignacion.empleado_id, 
                                    asignacion.horario_id,  horario.hora_entrada, horario.hora_salida, asignacion.turno,asignacion.dia')
                           ->join('horario','asignacion.horario_id=horario.horario_id')
                           ->order_by('asignacion_id','desc')
                            ->where('empleado_id = ', $id)
                           ->limit(2)
                           ->get('asignacion');
 
      
       if($query->num_rows() > 0){
             foreach($query->result() as $row){
                 $data[] = $row;
             }
             return $data;
         }
         else{
             return 0;
         }
     }
     function obtener_dosultimas_es($id)   
     {
       
         $query = $this->db->select ('es.es_id, es.hora, es.bono_noc, es.empleado_id, es.tipo')
                           ->order_by('es_id','desc')
                            ->where('empleado_id = ', $id)
                           ->limit(1)
                           ->get('es');
 
      
       if($query->num_rows() > 0){
             foreach($query->result() as $row){
                 $data[] = $row;
             }
             return $data;
         }
         else{
             return 0;
         }
     }
   
    function max_asig($id)
    {
        $query = $this->db->select ('MAX(asignacion.asignacion_id) as id')
                                 ->where('empleado_id = ', $id)
                                 ->get('asignacion');
                         
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function asignacion($id) //USO
    {
        $query = $this->db->select ('asignacion.asignacion_id, asignacion.fecha,asignacion.horario_id, asignacion.empleado_id, horario.nombre, 
                                    asignacion.horario_id, asignacion.pernocta, horario.hora_entrada, horario.hora_salida, asignacion.turno,
                                    horario.tipo')
                                 ->join('horario','asignacion.horario_id=horario.horario_id')
                                 ->where('empleado_id = ', $id)
                                 ->where('fecha', date('Y-m-d'))
                                 ->get('asignacion');
                         
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

   

   function obtener_tipo_por_fecha($fe, $id)
   {     
       $query = $this->db->select('es.tipo as tipo, es.fecha as fecha, es.empleado_id as empleado_id')
                         ->where('fecha = ',  $fe)
                         ->where('empleado_id = ', $id)
                        // ->where('empleado_id = ', '1')
                         ->get('es');


    
     if($query->num_rows() > 0){
           foreach($query->result() as $row){
               $data[] = $row;
           }
           return $data;
       }
       else{
           return 0;
       }
   }
   
    function obtener_por_empleado($_id)   
     {
       
         $query = $this->db
                           ->where('empleado_id = ', $_id)
                           ->order_by('fecha','desc')
                           ->limit(30)
                           ->get('es');
 
      
       if($query->num_rows() > 0){
             foreach($query->result() as $row){
                 $data[] = $row;
             }
             return $data;
         }
         else{
             return 0;
         }
     }
     
     function obtener_por_empleado_por_fecha($_id,$fecha)   
     {
         $query = $this->db->select('*')
                           ->where('empleado_id = ', $_id)
                           ->where('fecha = ', $fecha)
                           ->order_by('tipo asc')
                           ->get('es');
 
      
       if($query->num_rows() > 0){
             foreach($query->result() as $row){
                 $data[] = $row;
             }
             return $data;
         }
         else{
             return 0;
         }
     }
     
    function obtener_por_departamento_id($dep){

        $query = $this->db->select(' es.es_id as es_id, es.empleado_id AS empleado_id, es.hora as hora, es.fecha as fecha,  es.dia as dia,
                                   es.tipo as tipo, empleado.departamento_id as departamento_id,
                                    departamento.nombre AS dep_nombre, empleado.nombre as nombre, es.tiempo_extra as tiempo_extra, es.tiempo_extra_noche as tiempo_extra_noche')
                          ->join('empleado','es.empleado_id = empleado.empleado_id','INNER')
                          ->join('departamento','empleado.departamento_id = departamento.departamento_id','INNER')
                          ->where('departamento.departamento_id = ', $dep)
                           ->where('es.tipo = ', 'Entrada')
                          ->get('es');
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_departamento_id_noti($dep){

        $query = $this->db->select(' fallas_empl.falla_id, fallas_empl.empleado_id AS empleado_id, fallas_empl.hora as hora, fallas_empl.fecha as fecha,   fallas_empl.dia as dia,
                                    fallas_empl.tipo as tipo,  fallas_empl.descripcion, empleado.departamento_id as departamento_id,
                                    departamento.nombre AS dep_nombre, empleado.nombre as nombre')
                          ->join('empleado','fallas_empl.empleado_id = empleado.empleado_id','INNER')
                          ->join('departamento','empleado.departamento_id = departamento.departamento_id','INNER')
                          ->where('departamento.departamento_id = ', $dep)
                          ->get('fallas_empl');
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
     



     function obtener_por_id_es($_id,$fecha)   //controlador detalle
    {
      
      $query = $this->db->select('fotos.foto , es.es_id , es.fecha as fecha, es.hora as hora, es.dia as dia, es.tipo as tipo, es.bono_noc as bono_noc, 
                                es.tiempo_extra as tiempo_extra, es.tiempo_extra_noche as tiempo_extra_noche, es.empleado_id as empleado_id, empleado.nombre as nombre, empleado.cedula as cedula')
                          ->join('fotos','fotos.es_id=es.es_id','LEFT')
                          ->join('empleado','es.empleado_id=empleado.empleado_id')
                          ->where('es.empleado_id = ', $_id)
                           ->where('es.fecha = ', $fecha)
                          ->get('es');
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }    

}