<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Estacion_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function insertar_estacion($_data){
        $data = array(
            
            'nombre'            => $_data->post('nombre'),
            'altura'       => $_data->post('altura'),
            'descripcion'       => $_data->post('descripcion')
    
        );
        if($this->db->insert('estacion',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function obtener_todos(){
        $query = $this->db->get('estacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_id($_id){
        $query = $this->db->where('estacion_id',$_id)->get('estacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function lastid(){
        $query = $this->db->select('MAX(estacion_id) AS estacion_id')
                          ->get('estacion');
        
            foreach($query->result() as $row){
                $data[] = $row;
            }
        if($query->num_rows() > 0){
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function actualizar_estacion($_data){
        
        $data = array(
            'nombre'            => $_data->post('nombre'),
            'altura'       => $_data->post('altura'),
            'descripcion'       => $_data->post('descripcion'),
        );
        
        if($this->db->where('estacion_id',$_data->post('estacion_id'))->update('estacion',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function borrar_estacion($id){
        
        if($this->db->where('estacion_id',$id)->delete('estacion')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
   }
