<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Asignacion_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function insertar_asignacion($_data){
        $data = array(
            'fecha'       => $_data->post('fecha'),
            'pernocta'    => $_data->post('pernocta'),
            'trabajo'     => $_data->post('trabajo'),
            'dia'         => $_data->post('dia'),
            'turno'       => $_data->post('turno'),
            'empleado_id' => $_data->post('empleado_id'),
            'horario_id'  => $_data->post('horario_id')
        );
        if($this->db->insert('asignacion',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function obtener_todos(){
        $query = $this->db->get('asignacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    
    function obtener_por_id($_id){
        $query = $this->db->where('asignacion_id',$_id)->get('asignacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_por_empleado_id($_id){
        $query = $this->db->order_by('fecha','desc')
                          ->limit(5)
                          ->where('empleado_id',$_id)
                          ->get('asignacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_todos_con_empleado_horario(){
        $query = $this->db->select('asignacion.fecha, asignacion.dia, asignacion.turno, empleado.nombre as empleado,
                                   asignacion.asignacion_id, horario.nombre')
                          ->join('empleado','asignacion.empleado_id = empleado.empleado_id','INNER')
                          ->join('horario','asignacion.horario_id = horario.horario_id','INNER')
                          ->where('asignacion.fecha =',date('Y-m-d'))
                          ->get('asignacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function lastid(){
        $query = $this->db->select('MAX(asignacion_id) AS id')
                          ->get('asignacion');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function actualizar_asignacion($_data){
        
        $data = array(
            'fecha'       => $_data->post('fecha'),
            'pernocta'    => $_data->post('pernocta'),
            'trabajo'     => $_data->post('trabajo'),
            'dia'         => $_data->post('dia'),
            'turno'       => $_data->post('turno'),
            'empleado_id' => $_data->post('empleado_id'),
            'horario_id'  => $_data->post('horario_id')
        );
        
        if($this->db->where('asignacion_id',$_data->post('asignacion_id'))->update('asignacion',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function borrar_asignacion($id){
        
        if($this->db->where('asignacion_id',$id)->delete('asignacion')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}	
