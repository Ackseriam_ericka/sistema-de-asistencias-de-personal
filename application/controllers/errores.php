<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once('login.php');
class Errores extends Login {


    public function __construct()
    { 
	    parent::__construct();
    }
    
    public function index()
    {
		
    }
	public function error_404()
	{
		$this->load->view('error_404');
	}
	
	public function no_js()
	{
		$this->load->view('no_js');
	}
}
													