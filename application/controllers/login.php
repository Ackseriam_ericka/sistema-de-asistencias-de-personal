<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *Clase de Login.
 *
 *
 *La clase Login o controlador Login, contiene las funciones que son necesarias para realizar el
 *inicio de sesión en el sistema, así mismo contiene las funciones que son referentes a la sessión
 *del usuario en el sistema.
 *
 *@author Joeinny Osoario
 *@author Jesús Vielma
 *@package Controllers
 */
class Login extends CI_Controller {


    public function __construct()
    { 
	    parent::__construct(); 
		$this->load->model('Login_Model');
		$this->load->model('Usuario_Model');
		
		$this->load->library('form_validation');
	}
	
	/**
	 *Funcion index()
	 *
	 *@ignore
	 *Este es el index de controlador Login
	 */
	public function index()
	{
		$this->login();
	}
	
	/**
	 *Función login().
	 *
	 *La función login se encarga de realizar el inicio de sesión en el sistema
	 *@internal La función valida si existe una sesión creada, si la esta no
	 *existe se muestra el formulario de inicio de sesión, luego se consultan los datos
	 *introducidos con la base de datos y si estos existen se crea la sesión del usuario con
	 *los datos necesarios.
	 */
	public function login()
	{
		if ($this->session->userdata('usuario') == false) : 
		//Validar los campos del formulario
		$this->form_validation->set_rules('username', 'Usuario', 'required|trim|xss_clean');
		$this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean|md5');
 
    if ($this->form_validation->run() == false) :
	 $data['error']='No';
      $this->load->view('login/index',$data);
    else:
      //Asignar variables a lo obtenido desde el formulario
      $usr = $this->input->post('username');
      $pass = ($this->input->post('password'));
 
      $ingreso = $this->Login_Model->login($usr, $pass);
 
      switch ($ingreso):
        case 0:
          { $data['error'] = 'Si';
		  
			$this->load->view('login/index',$data);}	
        break;
        case 1:
           {
			$fila= $this->Login_Model->obtener_rol($usr);
			$this->session->userdata('usuario');
			$sess_array = array();
			$sess_array = array(
            'usuario'    => $fila[0]->usuario,
			'rol'         => $fila[0]->niveles,
			'cargo'	      => $fila[0]->cargo,
			'departamento'=> $fila[0]->departamento_id,
			'nombre'=> $fila[0]->nombre,
			'id'=> $fila[0]->empleado_id,
          );
		   $this->session->set_userdata('usuario', $sess_array);
		   $this->session->set_flashdata('inicio','1');
          redirect('home',	$data);
				
			       break;}
      endswitch;
    endif;
  endif;
  }

	/**
	 *Función logout().
	 *
	 *Esta función se encarga de deshacer y destruir la sesión que se habia creada para
	 *el usuario.
	 */
	public function logout()
	{

		$this->session->unset_userdata('usuario');
		$this->session->sess_destroy();
		
		$this->index();
	}
	/**
	 *Funcion check_session().
	 *
	 *Función utilizada para verificar que el usuario tiene un sesión creada.
	 *@method boolean userdata() El método userdata(), heradado de la libreria session es el encargado
	 *de verificar que existe la sesión
	 *@internal Si la sesiín no existe o ya ha expierado, se redirecciona al controlador para relaizar 
	 *el inico de sesion.
	 */
	public function check_session()
	{
		if(!$this->session->userdata('usuario'))
		{


			redirect('/login/','refresh');

			redirect('login','refresh');

			redirect('login','refresh');

		}
	}
    
	public function recuperar()
    {
		$this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');
		
    if ($this->form_validation->run() == false) 
	 {$data['error']='No';
      $this->load->view('recuperar/index',$data);}
    else
      {//Asignar variables a lo obtenido desde el formulario
      $email = $this->input->post('email');
   
      $valido = $this->Login_Model->recuperar($email);
 
      switch ($valido):
        case 0:
          {
			$data['error'] = 'Si';
			$this->load->view('recuperar/index',$data);
			}
			 break;	
		case 1:
           {
			   $data['fila']= $this->Login_Model->obtener_por_id($email);
	  		$this->load->view('recuperar/contrasena',$data); //controlador
			
			break;
			}
      endswitch;}
  }
  
  
   public function cambioConstrasena($id, $phase=1)
    {
		$data['fila'] = $this->Login_Model->obtener_por_id($email, $id);
	if ($phase==1)
	{
	    
	    $data['titulo'] = 'CHAP - Login';
	    $data['controlador'] = 'Editar';
	    if($data['fila']==0)
	    {
		$this->load->view('recuperar/contrasena',$data);
	    }
	    
	    $this->load->view('recuperar/contrasena',$data);
	    
	}
	else
	{
	   
	    $this->form_validation->set_rules('contrasena', 'Contraseña', 'required|md5');
	    $this->form_validation->set_rules('confcontrasena', 'Confirmacion de contraseña', 'required|matches[contrasena]');
	   
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    $this->form_validation->set_message('matches', 'El campo %s  debe coincidir');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->cambioConstrasena($id, 1);
	    }
	    else
	    {					
		if ($this->Login_Model->actualizar_contrasena($this->input))
		{
		    redirect('login/index', 'refresh');
		}
		else
		{
		    redirect('login/index/'.$id.'/1', 'refresh');   
		}
	    }
	}
    }
   
}
