<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 *Controlador Departamento.
 *
 *
 *El controlador Departamento se encarga de realizar la funciones CRUD (Create, Read, Update, Delete)
 *para el modulo de Departamentos. Así mismo contiene los métodos necesarios para obtener y enviar los
 *datos necesarios en los formularios.
 *
 *@author Ericka Simancas
 *@package Controllers
 */

class Departamento extends Login {


    public function __construct()
    { 
	    parent::__construct();
	    
	    $this->load->model('Departamento_Model');
	    $this->load->model('Empleado_Model');
    }

    /**
	 * Funcion index().
	 *
	 * Utlizada para cargar por defecto la pagina de inicio del controlador, es decir esta función
	 * es la encargada de mostrar el listado de los departamentos.
	 */
    public function index()
    {
	    $data['titulo'] = 'CHAP - Departamentos';
	    $data['controlador'] = 'Listar';
	    $data['filas'] = $this->Departamento_Model->obtener_todos();
    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('departamento/index');
	    $this->load->view('base/pie');
    }
	
    public function listar_por_departamento($dep=0)
	{
	    $this->check_session();
	    $data['titulo'] = 'CHAP - Departamentos';
	    $data['controlador'] = 'Listar';
	
       $data['filas'] = $this->Empleado_Model->obtener_por_departamento_id($dep);
        

		$this->load->view('base/cabecera',$data);
	    $this->load->view('departamento/listar_departamento');
		$this->load->view('base/pie');
	} 
  
    /**
	 * Funcion insertar().
	 * 
	 * Funcion para realizar: la carga de las vista de inserción, validación de campos e insercion
	 * en base de datos
	 * @param int $phase Parametro que define en cual de fase se encunetra la función.
	 *
	 * @internal Si la $phase==1 se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos, por el contrario se mostrara
	 * de nuevo el formulario de inserciín con los errores correspondientes.
	 */
	public function insertar($phase=1)
	{
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');
		$data['empleados'] = $this->Empleado_Model->obtener_todos();
		$data['titulo'] = 'CHAP - Departamentos';
		$data['controlador'] = 'Insertar';
		if ($phase==1) {
			
			
			$this->load->view('base/cabecera',$data);
			$this->load->view('departamento/insertar');
			$this->load->view('base/pie');
		}
		else
		{
			$this->form_validation->set_rules('nombre', 'Nombre', 'required');
			$this->form_validation->set_rules('responsable', 'Responsable', 'required');
			$this->form_validation->set_rules('descripcion','Descripción','required');
			
			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			
			if($this->form_validation->run()== FALSE )
			{
			$this->insertar(1);
			}
			else
			{
				if ($this->Departamento_Model->insertar_departamento($this->input))
				{
					redirect('departamento', 'refresh');
				}
				else
				{
					redirect('departamento/insertar', 'refresh');   
				}
			}
		}       
	}
    
    /**
	 *Funcion editar().
	 *
	 * Función dedicada a la carga de los datos para la vista de edicion, validación de campos
	 * y modificación del registro en la base de datos
	 * @param int $id Identificador del empleado en la base de datos 
	 * @param inf $phase Parametro que define en cual de fase se encuentra la función.
	 *
	 * @internal Si la $phase==1 Se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos por el contrario se mostrara 
	 * de nuevo el formulario de inserción con los errores correspondientes.
	 */
    public function editar($id, $phase=1)
	{
		if ($phase==1)
		{
			$data['fila'] = $this->Departamento_Model->obtener_por_id($id);
			$data['empleados'] = $this->Empleado_Model->obtener_por_departamento_id($id);
		    $data['titulo'] = 'CHAP - Departamentos';
		    $data['controlador'] = 'Editar';
				    
			if($data['fila']==0)
			{
				redirect('departamento','refresh');
			}
			    
			$this->load->view('base/cabecera',$data);
			$this->load->view('departamento/editar');
			$this->load->view('base/pie');
		}
		else
		{
			$this->form_validation->set_rules('nombre', 'Nombre', 'required');
		    $this->form_validation->set_rules('responsable', 'Supervisor', 'required');
		    $this->form_validation->set_rules('descripcion','Descripcion','required');
			
			$this->form_validation->set_message('required', 'El campo %s  es requerido');
		   
		   if($this->form_validation->run()== FALSE )
		   {
				$this->editar(1,$id);
			}
		    else
		    {
			    if ($this->Departamento_Model->actualizar_departamento($this->input))
			    {
				 redirect('departamento/index', 'refresh');
			    }
			    else
				{
				    redirect('departamento/editar/'.$id.'/1', 'refresh');   
				}
		    }
		}
    }
  
    /**
	 *Funcion borrar().
	 *
	 *Borra el registro de la asignación de la base de datos.
	 *@param int $id Identificador del empleado en la base de datos
	 */
    public function borrar($id)
    {
        $this -> Departamento_Model -> borrar_departamento($id);
        redirect('departamento', 'refresh');
    }

    /**
	 *Funcion detalle().
	 *
	 *Función usada para mostrar la vista detallada de la información del departamento
	 *@param int $id Identificador del empleado, usado para realizar la consulta de la información del departamento
	 */
	public function detalle($id)
    {
    $data['fila']    =  $this->Departamento_Model->obtener_por_id($id);
	$data['empleados'] = $this->Empleado_Model->obtener_por_departamento_id($id);
	$data['titulo'] = 'CHAP - Departamentos';
	$data['controlador'] = 'Listar';
    
	$this->load->view('base/cabecera',$data);
	$this->load->view('departamento/detalle');
	$this->load->view('base/pie');
    }
}