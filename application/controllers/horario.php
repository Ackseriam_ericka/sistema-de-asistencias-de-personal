<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 *Controlador Horario.
 *
 *El controlador Horario se encarga de realizar la funciones CRUD (Create, Read, Update, Delete)
 *para el modulo de Horario. La funcionalidad del controlador horario no debe confurdirse con la
 *del controlador asignación puesto que el controlador Horario se encarga de manejar especificamente
 *el horario, es decir, horaras de entrada, salida, dias de funcionamiento, entre otros.
 *
 *@author Jesús Vielma
 *@package Controllers
 */
class Horario extends Login {


    public function __construct()
    { 
	    parent::__construct();
	    
	    $this->load->model('Horario_Model');
    }
    
	/**
	 * Funcion index().
	 *
	 * Utlizada para cargar por defecto la pagina de inicio del controlador, es decir esta función
	 * es la encargada de mostrar el listado de las asignaciones creadas para los empleados.
	 */
    public function index()
    {
	    $this->check_session();
	    $data['filas']       = $this->Horario_Model->obtener_todos();
	    $data['titulo']      = 'CHAP - Horario';
	    $data['controlador'] = 'Listar';
	    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('horario/index');
	    $this->load->view('base/pie');
    }
    
	/**
	 * Funcion insertar().
	 * 
	 * Funcion para realizar: la carga de las vista de inserción, validación de campos e insercion
	 * en base de datos
	 * @param int $phase Parametro que define en cual de fase se encunetra la función.
	 *
	 * @internal Si la $phase==1 se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos, por el contrario se mostrara
	 * de nuevo el formulario de inserciín con los errores correspondientes.
	 */
    public function insertar($phase=1)
    {
		$this->check_session();
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');
		$data['titulo']      = 'CHAP - Horario';
		$data['controlador'] = 'Insertar';
		
		if ($phase==1) {
			
			$this->load->view('base/cabecera',$data);
			$this->load->view('horario/insertar');
			$this->load->view('base/pie');
		}
		else
		{
			$this->form_validation->set_rules('hora_entrada', 'Hora de entrada', 'required|trim');
			$this->form_validation->set_rules('hora_salida', 'Hora de salida', 'required|trim');
			$this->form_validation->set_rules('descripcion','descripcion','required|trim');
			$this->form_validation->set_rules('tipo','Tipo','required|trim');
			$this->form_validation->set_rules('nombre','Nombre del Horario','required|trim');
			$this->form_validation->set_rules('dias','Días','required|trim');
			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			
			if($this->form_validation->run()== FALSE )
			{
			$this->insertar(1);
			}
			else
			{
				
				if ($this->Horario_Model->insertar_horario($this->input))
				{
					redirect('horario/index', 'refresh');
				}
				else
				{
					redirect('horario/insertar_horario', 'refresh');   
				}
			}
		}
    }
    
	/**
	 *Funcion editar().
	 *
	 * Función dedicada a la carga de los datos para la vista de edicion, validación de campos
	 * y modificación del registro en la base de datos
	 * @param int $id Identificador del empleado en la base de datos 
	 * @param inf $phase Parametro que define en cual de fase se encuentra la función.
	 *
	 * @internal Si la $phase==1 Se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos por el contrario se mostrara 
	 * de nuevo el formulario de inserción con los errores correspondientes.
	 */
    public function editar($id, $phase=1)
    {
		$this->check_session();
		if ($phase==1)
		{
			$data['fila'] = $this->Horario_Model->obtener_por_id($id);
			$data['titulo'] = 'CHAP - Horario';
			$data['controlador'] = 'Editar';
			
			if($data['fila']==0)
			{
			redirect('horario/index','refresh');
			}
			
			$this->load->view('base/cabecera',$data);
			$this->load->view('horario/editar');
			$this->load->view('base/pie');
		}
		else
		{
			$this->form_validation->set_rules('hora_entrada', 'Hora de entrada', 'required|trim');
			$this->form_validation->set_rules('hora_salida', 'Hora de salida', 'required|trim');
			$this->form_validation->set_rules('descripcion','descripcion','required|trim');
			$this->form_validation->set_rules('tipo','Tipo','required|trim');
			$this->form_validation->set_rules('nombre','Nombre del Horario','required|trim');
			$this->form_validation->set_rules('dias','Días','required|trim');
			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			
			if($this->form_validation->run()== FALSE )
			{
			$this->editar(1,$id);
			}
			else
			{
				if ($this->Horario_Model->actualizar_horario($this->input))
				{
					redirect('horario/index', 'refresh');
				}
				else
				{
					redirect('horario/editar/'.$id.'/1', 'refresh');   
				}
			}			
		}
    }
    
	/**
	 *Funcion detalle().
	 *
	 *Función usada para mostrar la vista detallada de la información del empleado
	 *@param int $id Identificador del empleado, usado para realizar la consulta de la información del empleado
	 *
	 *@internal Este función no esta en funcionamiento puesto que no hay datos relevantes que mostrar
	 *que no se muestren en el index del controlador.
	 */
    public function detalle($id)
    {
	$data['fila']    =  $this->Horario_Model->obtener_por_id($id);
	$this->load->view('detalle_horario',$data);
    }
    
	/**
	 *Funcion borrar().
	 *
	 *Borra el registro de la asignación de la base de datos.
	 *@param int $id Identificador del empleado en la base de datos
	 */
    public function borrar($id)
    {
		$this->check_session();
        $this -> Horario_Model -> borrar_horario($id);
        redirect('horario', 'refresh');
    }
}