<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 * Controlador emmpleado.
 *
 *
 * Encargada de manejar el modulo de emplados, con el cual se puede acceder a un
 * listado completado de todos los trabajadores activos de la empresa (funcion index), así mismo
 * tiene la funcionaldiad para hacer los llamados necesarios para hacer la creación, edición y borrado
 * de los empelados, así mismo contiene las funciones necesarias para la creación automatica de un
 * codigo único para cada empleado, la otra función se encarga derealizar la carga de una fotografía
 * que sera asosicada al cliente.
 *
 * @var array . $data['titulo'] Define  el titulo para la vista que se cargara 
 * @var mixed . $data['controlador'] Define una variable que sera usada para hacer solo los llamados necesarios
 * de java script en el archivo base/pie.php
 * @var mixed . $data['cargos] Guarda el resusltado de una consulta a base de datos, la cual debería contener los
 * cargos que la empresa tiene
 * @var mixed . $data['departamentos'] Guarda el resusltado de una consulta a base de datos, la cual debería contener los
 * departamentos de la empresa.
 * @var mixed . $data['filas'] Guarda el resusltado de una consulta a base de datos, la cual debería contener la
 * consulta principal de la función
 * 
 * @author Jesús Vielma
 * @package Controllers
 */
class Empleado extends Login {
	/**
	 * Metodo contrustor, así mismo se encarga de cargar todos los
	 * modelos, liberias y helpers necesarios para el controlador.
	 */
    public function __construct()
    { 
	    parent::__construct();
	    date_default_timezone_set('America/Caracas');
	    $this->load->model(array('Empleado_Model','Cargo_Model','Departamento_Model','Asignacion_Model','Marcaje_Model'));
    }
	/**
	 * Funcion index().
	 *
	 * Utlizada para cargar por defecto la pagina de inicio del controlador, es decir esta función
	 * es la encargada de mostrar el listado de los empleados de la empresa
	 * @method check_session de la clase Login se usa para ver la sesion creada para el usuario
	 * @param int $dep Parametro que define el departamento con el cual se extraeran los empleados
	 * 
	 * @internal Si el parametro $dep es igual a cero se extraeran todos los emeplados, si es diferente de 0 se
	 * extraeran todos los empleados cuyos registros contengan en id del departamento
	 */
    public function index($dep=0)
    {
	    $this->check_session();
		$data['titulo'] = 'CHAP - Empleados';
	    $data['controlador'] = 'Listar';
		$data['departamentos'] = $this->Departamento_Model->obtener_todos();
		if($dep==0)
	    $data['filas'] = $this->Empleado_Model->obtener_todos();
		else
		$data['filas'] = $this->Empleado_Model->obtener_por_departamento_id($dep);
		
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('empleado/index');
	    $this->load->view('base/pie');
    }
	/**
	 * Funcion insertar().
	 * 
	 * Funcion para realizar: la carga de las vista de inserción, validación de campos e insercion
	 * en base de datos
	 * @param int $phase Parametro que define en cual de fase se encunetra la función.
	 *
	 * @internal Si la $phase==1 se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos, por el contrario se mostrara
	 * de nuevo el formulario de inserciín con los errores correspondientes.
	 */
    public function insertar($phase=1)
    {
		$this->check_session();
		
		if ($this->session->userdata('usuario')['rol']==3 || $this->session->userdata('usuario')['rol']==2)
		{
		?>
		<script>
			alert('No tiene permisos para ingresar un nuevo empleado esta es una función correspondiente a la oficina de Talento Humano.');
			location.href="<?=site_url('empleado/index/'.$this->session->userdata('usuario')['departamento'])?>"
		</script>
		<?php
			//redirect('empleado');	
		}
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');
		$data['cargos']        = $this->Cargo_Model->obtener_todos();
		$data['departamentos'] = $this->Departamento_Model->obtener_todos();
		$data['titulo'] = 'CHAP - Empleados';
		$data['controlador'] = 'Insertar Empleado';
		$data['error_subida'] = '';
		if ($phase==1) {
			
			$this->load->view('base/cabecera',$data);
			$this->load->view('empleado/insertar');
			$this->load->view('base/pie');
		}
		else
		{
			$this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
			$this->form_validation->set_rules('cedula', 'Cedula', 'trim|required|is_unique[empleado.cedula]|xss_clean');
			$this->form_validation->set_rules('fecha_nac','Fecha de Nacimiento','trim|required');
			$this->form_validation->set_rules('fecha_ingreso','Fecha de Nacimiento','trim|required');
			$this->form_validation->set_rules('codigo_empl','Codigo del empleado','trim|required');
			$this->form_validation->set_rules('contrato','Tipo de contrato','trim|required');
			$this->form_validation->set_rules('cargo_id','Cargo','trim|required');
			$this->form_validation->set_rules('departamento_id','Departamento','trim|required');
			$this->form_validation->set_rules('estacion_id','Estacion','trim|required');
		
			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			$this->form_validation->set_message('is_unique', 'El campo %s ya existe, ingrese uno nuevo');
			
			if($this->form_validation->run()== FALSE )
			{
			$this->insertar(1);
			}
			else
			{
					if ($this->Empleado_Model->insertar_empleado($this->input))
					{
						/*$file_info = $this->upload->data();
						$imagen = $file_info['file_name'];
						$id = $this->Empleado_Model->lastid();
						$this->Empleado_Model->direccion_imagen($imagen,$id[0]->id);*/
						
						//redirect('empleado/index', 'refresh');
						$id_empl_insert = $this->Empleado_Model->lastid();
						redirect('asignacion/insertar/1/'.$id_empl_insert[0]->id, 'refresh');
					}
					else
					{
						redirect('empleado/insertar/1', 'refresh');   
					}
			}
		}
    }
	/**
	 *Funcion editar().
	 *
	 * Función deducada a la carga de los datos para la vista de edicion, validación de campos
	 * y modificación del registro en la base de datos
	 * @param int $id Identificador del empleado en la base de datos 
	 * @param inf $phase Parametro que define en cual de fase se encuentra la función.
	 *
	 * @internal Si la $phase==1 Se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos por el contrario se mostrara 
	 * de nuevo el formulario de inserción con los errores correspondientes.
	 */
    public function editar($id, $phase=1)
    {
		$this->check_session(); //en cada funcion
		if ($this->session->userdata('usuario')['rol']==3 || $this->session->userdata('usuario')['rol']==2)
		{
		?>
		<script>
			alert('No tiene permisos para acceder al modulo de modificación de datos del empleado.');
			location.href="<?=site_url('empleado/index/'.$this->session->userdata('usuario')['departamento'])?>"
		</script>
		<?php
			//redirect('empleado');	
		}
		
		$data['fila'] = $this->Empleado_Model->obtener_por_id($id);
		$data['cargos'] = $this->Cargo_Model->obtener_todos();
		$data['departamentos'] = $this->Departamento_Model->obtener_todos();
		$data['titulo'] = 'CHAP - Empleados';
		$data['controlador'] = 'Editar Empleado';
		
		if ($phase==1)
		{
			
			if($data['fila']==0)
			{
			redirect('empleado/index','refresh');
			}
			$data['error_subida'] = '';
			$this->load->view('base/cabecera',$data);
			$this->load->view('empleado/editar');
			$this->load->view('base/pie');
		}
		else
		{
			$this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
			$this->form_validation->set_rules('cedula', 'Cedula', 'trim|required');
			$this->form_validation->set_rules('fecha_nac','Fecha de Nacimiento','trim|required');
			$this->form_validation->set_rules('codigo_empl','Codigo del empleado','trim|required');
			$this->form_validation->set_rules('cargo_id','Cargo','trim|required');
			$this->form_validation->set_rules('contrato','Contrato','trim|required');
			$this->form_validation->set_rules('departamento_id','Departamento','trim|required');
			$this->form_validation->set_rules('estacion_id','Estacion','trim|required');
			//$this->form_validation->set_rules('imagen','Fotografia','required');
			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			
			if($this->form_validation->run()== FALSE )
			{
			$this->editar($id,1);
			}
			else
			{		
				if ($this->Empleado_Model->actualizar_empleado($this->input))
				{						
					redirect('empleado/index', 'refresh');
				}
				else
				{
					redirect('empleado/editar/'.$id.'/1', 'refresh');   
				}
			}	
		}
    }
	/**
	 *Funcion detalle().
	 *
	 *Función usada para mostrar la vista detallada de la información del empleado
	 *@param int $id Identificador del empleado, usado para realizar la consulta de la información del empleado
	 *
	 */
    public function detalle($id)
    {
		$this->check_session();
		$data['fila']   =  $this->Empleado_Model->obtener_empleado_con_estac_depto_cargo($id);
		$data['asignaciones'] = $this->Asignacion_Model->obtener_por_empleado_id($id);
		$data['marcajes'] = $this->Marcaje_Model->obtener_por_empleado($id);
		$data['titulo'] = 'CHAP - Empleados';
		$data['controlador'] = 'Detalle';
		$data['error_subida'] = '';
		
		$this->load->view('base/cabecera',$data);
		$this->load->view('empleado/detalle');
		$this->load->view('base/pie');
    }
	/**
	 *Funcion get_code().
	 *
	 *Funcion que se utiliza cuando se crea o editar el codigo de un empleado, mediante formularios, recibe con el metodo POST
	 *el un string con el nombre del empleado, para luego generar un codigo unico que luego sera devuelto
	 *al formulario
	 */
    public function get_code()
    {
		if($this->input->post('name'))
		{
			$name = trim($this->input->post('name'));
			$id = trim($this->input->post('id'));
			$nombre= explode(" ",$name);
			
			if(($nombre[0]!="")&&(isset($nombre[1]))&&(isset($nombre[2]))&&(isset($nombre[3]))&&(isset($nombre[4])))
			{
				$ini = strtoupper($nombre[0]{0});
				$ini = $ini.strtoupper($nombre[1]{0});
				$ini = $ini.strtoupper($nombre[2]{0});
				$ini = $ini.strtoupper($nombre[3]{0});
				$ini = $ini.strtoupper($nombre[4]{0});
			}
			else
			{
				if(($nombre[0]!="")&&(isset($nombre[1]))&&(isset($nombre[2]))&&(isset($nombre[3])))
				{
					$ini = strtoupper($nombre[0]{0});
					$ini = $ini.strtoupper($nombre[1]{0});
					$ini = $ini.strtoupper($nombre[2]{0});
					$ini = $ini.strtoupper($nombre[3]{0});
				}
				else 
				{
					if (($nombre[0]!="")&&(isset($nombre[1]))&&(isset($nombre[2])))
					{
					$ini = strtoupper($nombre[0]{0});
					$ini = $ini.strtoupper($nombre[1]{0});
					$ini = $ini.strtoupper($nombre[2]{0});
					}
					else 
					{
						if (($nombre[0]!="")&&(isset($nombre[1])))
						{
						$ini = strtoupper($nombre[0]{0});
						$ini = $ini.strtoupper($nombre[1]{0});
						}
						else 
						{//echo "El nombre debe contener nombre y apellido";
							$ini= "";
							?>
							<script>alert("Debe ingresar el primer apellido");
							$("#nombre").focus();
							</script>
							<?php
						}
					}
				}
			}
			
			
			if($id==0)
				{
					$lastid = $this->Empleado_Model->lastid();
					$codigo_empl=$ini.''.($lastid[0]->id+1);
				}
				else
				{
					$codigo_empl=$ini.''.$id;
				}
				//$codigo_empl=$ini.''.(rand(1,999));
				?>
				<input type="text" value="<?=$codigo_empl?>" id="codigo_empl1" class="form-control" name="codigo_empl" readonly>
				<?php
		}
    }
    /**
	 *Funcion borrar().
	 *
	 *Borrar el registro del empleado de la base de datos, este funcion no esta implantada puesto
	 *que para efectos del sistema no debe utilizarse, sin embargo se crea como parte de los metodos
	 *CRUD de sistemas
	 *@param int $id Identificador del empleado en la base de datos
	 */
    public function borrar($id)
    {
        $this -> Empleado_Model -> borrar_empleado($id);
        redirect('empleado/index', 'refresh');
    }
	/**
	 *Funcion imagen().
	 *
	 *Funcion creada para la carga de una fotografia del empleado
	 *@param int $id Identificador del empleado
	 *@param string $codigo_empl recibe el codigo que tiene el empleado  
	 */
	public function imagen($id,$codigo_empl)
	{
		$config['upload_path'] = './assets/img/empleados/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $codigo_empl.".jpg";
		
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload())
		{
			$data['error_subida'] = $this->upload->display_errors();
			//$this->session->set_flashdata('error',$error);
			$data['fila']   =  $this->Empleado_Model->obtener_empleado_con_estac_depto_cargo($id);
			$data['asignaciones'] = $this->Asignacion_Model->obtener_por_empleado_id($id);
			$data['marcajes'] = $this->Marcaje_Model->obtener_por_empleado($id);
			$data['titulo'] = 'CHAP - Empleados';
			$data['controlador'] = 'Detalle';
			$this->load->view('base/cabecera',$data);
			$this->load->view('empleado/detalle');
			$this->load->view('base/pie');
		}
		else
		{
			$file_info = $this->upload->data();
			$imagen = $file_info['file_name'];
			if($this->Empleado_Model->direccion_imagen($imagen,$id))
			{
				redirect('empleado/detalle/'.$id);
			}
			else
			{
				redirect('empleado');
			}
		}
	}
}
