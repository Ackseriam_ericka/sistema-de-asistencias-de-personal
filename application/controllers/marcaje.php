<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php'); 
/**
 *Controlador Marcaje.
 *
 *El controlador Marcaje se encarga de realizar la funciones de la entrada y salida del personal 
 *Con el fin de obtener, los retardos, horas extras y fecha de ingreso y egreso
 *necesarios llenar el formulario con el codigo unico.
 *
 *@author Ericka Simancas
 *@package Controllers
 */

class Marcaje extends Login {


    public function __construct()
    { 
	    parent::__construct();
	    date_default_timezone_set('America/Caracas');
	   
	    $this->load->model('Marcaje_Model');
	    $this->load->model('Empleado_Model');
	    $this->load->model('Departamento_Model');
    }
    
    public function index()
    {
	    $data['titulo'] = 'CHAP - Marcaje';
	    $data['controlador'] = 'Listar';
	    
	    $data['filas'] = $this->Marcaje_Model->obtener_todos();
    
		
	    $this->load->view('marcaje/mostrar1_marcaje');

    }
    public function listar($dep=0)
    {

	    $this->check_session();
	    $data['titulo'] = 'CHAP - Marcaje';
	    $data['controlador'] = 'Listar';
	   if($dep==0){
	    $data['filas'] = $this->Marcaje_Model->obtener_todos();
	   $this->load->view('base/cabecera',$data);
	    $this->load->view('marcaje/index');
		$this->load->view('base/pie');

       	}else{
        $data['filas'] = $this->Marcaje_Model->obtener_por_departamento_id($dep);
		$this->load->view('base/cabecera',$data);
	    $this->load->view('marcaje/index');
		$this->load->view('base/pie');
	}
    }
     public function detalle_marcaje($dep)
    {

	    //$this->check_session();
	    $data['titulo'] = 'CHAP - Marcaje';
	    $data['controlador'] = 'Listar';
        $data['filas'] = $this->Marcaje_Model->obtener_por_departamento_id($dep);
        //var_dump($data);
		$this->load->view('base/cabecera',$data);
	    $this->load->view('marcaje/listar_marcaje');
		$this->load->view('base/pie');

    }

    public function listar_noti($dep=0)
    {

	    $this->check_session();
	    $data['titulo'] = 'CHAP - Notificaciones';
	    $data['controlador'] = 'Listar';
	   if($dep==0)
	    $data['filas'] = $this->Marcaje_Model->obtener_todos_noti();
       	else
       $data['filas'] = $this->Marcaje_Model->obtener_por_departamento_id_noti($dep);

		$this->load->view('base/cabecera',$data);
	    $this->load->view('marcaje/noti');
		$this->load->view('base/pie');

	

    }
   
	public function marcaje($phase=1)
	 {
	 	$data['titulo'] = 'CHAP - Marcaje';
	    $data['controlador'] = 'Insertar';
		if ($phase==1) {
		  
		   
		    $this->load->view('marcaje/mostrar1_marcaje');
		  
		   
		   }
	}   

	
    public function insertar_notificacion()
    {
		if (($this->Marcaje_Model->insertar_notificacion($this->input))and ($this->Marcaje_Model->insertar_marcaje($this->input)))
		{
		    redirect('webcam/index', 'refresh');
		}
		else
		{
		    redirect('insertar_falta', 'refresh');   
		}
	           
	}
  public function insertar()
    {  

		if ($this->Marcaje_Model->insertar_marcaje($this->input))
		{
		    redirect('webcam/index', 'refresh');
		}
		else
		{
		    redirect('insertar_marcaje', 'refresh');   
		}
	           
	}
    


	 public function detalle($id,$fecha)
    {
    $data['fila']   =  $this->Marcaje_Model->obtener_por_id_es($id,$fecha);
	$data['titulo'] = 'CHAP - Marcaje';
	$data['controlador'] = 'Detalle';
	$this->load->view('base/cabecera',$data);
	$this->load->view('marcaje/detalle');
	$this->load->view('base/pie');
    }
 }  