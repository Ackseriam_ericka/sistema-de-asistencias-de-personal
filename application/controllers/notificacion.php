<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php'); //colocarloooooo
/**
 *Controlador Notificacion.
 *
 *El controlador Notificacion se encarga de realizar las consultas necesarias a base de datos
 *para obtener todas las notificaciones que debens er mostradas al usuario.
 *
 *@author Ericka Simancas
 *@package Controllers
 */
class Notificacion extends Login
{
   public function __construct()
    { 
      parent::__construct();
        date_default_timezone_set('America/Caracas');
         $this->load->model('Notificacion_Model');
    }

    /**
	 * Funcion index().
	 *
	 * Utlizada para cargar por defecto la pagina de inicio del controlador, es decir esta función
	 * es la encargada de mostrar el listado de las asignaciones creadas para los empleados.
	 */
      public function index()
      { 
         $this->load->view('noti');
      }
      
      /**
       *Función getNoti().
       *
       *La función utiliza el metodo get del modelo Notificacion para obtener todas las notifiaciones
       *de las base de datos, luego las guarda en un array el cual sera llevado a formato json.
       */
      public function getNoti()
      {
         $datas = $this->Notificacion_Model->get();
         $obj=array();
         
         for($i=0;$i<count($datas);$i++)
         {
            
            $obj[] = array(
                           'id' => $datas[$i]->falla_id,
                           'descripcion' => $datas[$i]->descripcion,
                           'fecha' => $datas[$i]->fecha,
                           'status' => $datas[$i]->status,
                           'hora'=> $datas[$i]->hora,
                           'empleado_id' => $datas[$i]->empleado_id);
         }
         echo json_encode($obj);
      }

      /**
       *Funcion listar_noti().
       *
       *Se encarga de realizar un listado mucho mas especifico de las notificaciones en el sistema
       */
    public function listar_noti()
    {

      $this->check_session();
      $dep=$this->session->userdata('usuario')['departamento'];
      $data= $this->Notificacion_Model->obtener_por_departamento_id_noti($dep);

      echo json_encode($data);      
    }
    
    /**
     *Funcion actnoti()
     *
     *Se encarga de rellenar los datos necesarios para actualizar el estado dela notificación, "Leido" o "No Leido"
     */
    public function actnoti()
    {
      $_id=0;
      $id= $this->input->get('variable');
         $_id+= $id; //cambiar de formato string a int
       
        $data= $this->Notificacion_Model->obtener_por_id_noti($_id);
        $status=$data[0]->status;
        if($status=='No leido'){
          $status='Leido';
        }
        $status = array( 'status'=>$status,);
         $status_supervisor = array( 'status_supervisor'=>$data[0]->status_supervisor,);
        $descripcion = array('descripcion' => $data[0]->descripcion);
        $fecha = array('fecha' => $data[0]->fecha);
        $hora = array('hora' => $data[0]->hora);
        $dia  = array('dia' => $data[0]->dia);
        $empleado_id= array('empleado_id' => $data[0]->empleado_id);
        $tipo= array('tipo' => $data[0]->tipo);
        $oculto = array('falla_id' => $data[0]->falla_id);

               ?>
                <?= form_open(base_url() . 'index.php/notificacion/actualizar_datos','', $oculto) ?>
                <?= form_hidden($status) ?> 
                 <?= form_hidden($status_supervisor) ?>     
                <?= form_hidden($descripcion) ?>
                <?= form_hidden($fecha) ?>
                <?= form_hidden($hora) ?>
                <?= form_hidden($dia) ?>
                <?= form_hidden($empleado_id) ?>        
                <?= form_hidden($tipo) ?>
                <?php echo form_close()
                

                 ?>
                <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
                   <script>
            $(document).ready(function(){
                setTimeout(function(){
                    $('form').submit();
                    },001)
                })
        </script>
            <?php    
       }
       
       /**
     *Funcion actnoti_super()
     *
     *Se encarga de rellenar los datos necesarios para actualizar el estado dela notificación, "Leido" o "No Leido"
     *solo que en base al supervisor
     */
     public function actnoti_super()
    {
          $_id=0;
      $id= $this->input->get('variable');
         $_id+= $id; //cambiar de formato string a int
       
        $data= $this->Notificacion_Model->obtener_por_id_noti($_id);
        $status_supervisor=$data[0]->status_supervisor;
        if($status_supervisor=='No leido'){
          $status_supervisor='Leido';
        }
        $status_supervisor = array( 'status_supervisor'=>$status_supervisor,);
        $status = array( 'status'=> $data[0]->status);
        $descripcion = array('descripcion' => $data[0]->descripcion);
        $fecha = array('fecha' => $data[0]->fecha);
        $hora = array('hora' => $data[0]->hora);
        $dia  = array('dia' => $data[0]->dia);
        $empleado_id= array('empleado_id' => $data[0]->empleado_id);
        $tipo= array('tipo' => $data[0]->tipo);
        $oculto = array('falla_id' => $data[0]->falla_id);


               ?>
              <?= form_open(base_url() . 'index.php/notificacion/actualizar_datos','', $oculto) ?>
                <?= form_hidden($status_supervisor) ?>   
                <?= form_hidden($status) ?>    
                <?= form_hidden($descripcion) ?>
                <?= form_hidden($fecha) ?>
                <?= form_hidden($hora) ?>
                <?= form_hidden($dia) ?>
                <?= form_hidden($empleado_id) ?>        
                <?= form_hidden($tipo) ?>
                <?php echo form_close() 
                

                 ?>
                <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
                   <script>
            $(document).ready(function(){
                setTimeout(function(){
                    $('form').submit();
                    },001)
                })
        </script>
                <?php    
           }
           
      /**
       *Función actualizar_datos()
       *
       *Actualiza los datos en la base de datos
       */
       function actualizar_datos()    
               {        
                   $falla_id = $this->input->post('falla_id');
                   $descripcion = $this->input->post('descripcion');
                   $fecha = $this->input->post('fecha');
                   $hora = $this->input->post('hora');
                   $dia = $this->input->post('dia');
                   $empleado_id = $this->input->post('empleado_id');
                   $tipo = $this->input->post('tipo');
                   $status = $this->input->post('status');
                   $status_supervisor = $this->input->post('status_supervisor');
     
               if ($this->Notificacion_Model->actualizar_status($this->input))
              {
               redirect('marcaje/listar_noti', 'refresh');
                }else 
                {
                  echo "No";
                }
             
       }
}