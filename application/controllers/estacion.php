<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 *Controlador Estacion.
 *
 *
 *El controlador Estación se encarga de realizar la funciones CRUD (Create, Read, Update, Delete)
 *para el modulo de estación. Así mismo contiene los métodos necesarios para obtener datos 
 *necesarios en los formularios.
 *
 *@author Fernando Araque 
 *@package Controllers
 */
class Estacion extends Login {

    public function __construct()
    { 
	    parent::__construct();
	    
	    $this->load->model('Estacion_Model');
    }

    /**
	 * Funcion index().
	 *
	 * Utlizada para cargar por defecto la pagina de inicio del controlador, es decir esta función
	 * es la encargada de mostrar el listado de las estaciones de trabajo de los empleados.
	 */
    public function index()
    {
		$this->check_session();
	    $data['titulo'] = 'CHAP - Estacion';
	    $data['controlador'] = 'Listar';
	    $data['filas'] = $this->Estacion_Model->obtener_todos();
    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('estacion/index');
	    $this->load->view('base/pie');
    }
  
	/**
	 * Funcion insertar().
	 * 
	 * Funcion para realizar: la carga de las vista de inserción, validación de campos e insercion
	 * en base de datos
	 * @param int $phase Parametro que define en cual de fase se encunetra la función.
	 *
	 * @internal Si la $phase==1 se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos, por el contrario se mostrara
	 * de nuevo el formulario de inserciín con los errores correspondientes.
	 */
    public function insertar($phase=1)

{
	//$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');
    $this->check_session();

	$data['titulo'] = 'CHAP - Estacion';
    $data['controlador'] = 'Insertar';
	if ($phase==1) {
	    
	    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('estacion/insertar');
	    $this->load->view('base/pie');
	}
	else
    {
	    $this->form_validation->set_rules('nombre', 'Nombre', 'required');
	    $this->form_validation->set_rules('altura', 'Altura', 'required');
	    $this->form_validation->set_rules('descripcion','Descripcion','required');
   	   
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->insertar(1);
	    }
	    else
	    {
		
		if ($this->Estacion_Model->insertar_estacion($this->input))
		{
		    redirect('estacion', 'refresh');
		}
		else
		{
		    redirect('estacion/insertar', 'refresh');   
		}
	    }
    }       
	}
    
    /**
	 *Funcion editar().
	 *
	 * Función dedicada a la carga de los datos para la vista de edicion, validación de campos
	 * y modificación del registro en la base de datos
	 * @param int $id Identificador del empleado en la base de datos 
	 * @param inf $phase Parametro que define en cual de fase se encuentra la función.
	 *
	 * @internal Si la $phase==1 Se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos por el contrario se mostrara 
	 */
    public function editar($id, $phase=1)
     {
		  $this->check_session();
			if ($phase==1)
			{
			    $data['fila'] = $this->Estacion_Model->obtener_por_id($id);
			    $data['titulo'] = 'CHAP - Estacion';
			    $data['controlador'] = 'Editar';
			
			    
			    if($data['fila']==0)
			    {
				redirect('estacion','refresh');
			    }
			    
			    $this->load->view('base/cabecera',$data);
			    $this->load->view('estacion/editar');
			    $this->load->view('base/pie');
			}
			else
			{
				 $this->form_validation->set_rules('nombre', 'Nombre', 'required');
			    $this->form_validation->set_rules('altura', 'Altura', 'required');
			    $this->form_validation->set_rules('descripcion','Descripcion','required');
		   	   
		       if($this->form_validation->run()== FALSE )
		       	 {
				$this->editar(1,$id);
			    }
			    else
			    {

				    if ($this->Estacion_Model->actualizar_estacion($this->input))
				    {
					 redirect('estacion/index', 'refresh');
				    }
				    else
					{
					    redirect('estacion/editar/'.$id.'/1', 'refresh');   
					}
			    }
		    }
    
    }
  
    /**
	 *Funcion borrar().
	 *
	 *Borra el registro de la asignación de la base de datos.
	 *@param int $id Identificador del empleado en la base de datos
	 */
    public function borrar($id)
    {
        $this -> Estacion_Model -> borrar_estacion($id);
        redirect('estacion', 'refresh');
    }
	
	/**
	 *Funcion detalle().
	 *
	 *Función usada para mostrar la vista detallada de la información de la estacion
	 *@param int $id Identificador del empleado, usado para realizar la consulta de la información del empleado
	 *
	 *@internal Este función no esta en funcionamiento puesto que no hay datos relevantes que mostrar
	 *que no se muestren en el index del controlador.
	 */
    public function detalle($id)
    {

	$this->check_session();
	$data['fila']    =  $this->Estacion_Model->obtener_por_id($id);
	$data['titulo'] = 'CHAP - Estacion';
	$data['controlador'] = 'Detalle';
    
	$this->load->view('base/cabecera',$data);
	$this->load->view('estacion/detalle');
	$this->load->view('base/pie');
    }
}