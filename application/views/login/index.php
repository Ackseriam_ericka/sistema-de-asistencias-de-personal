<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="57x57"   href="<?=base_url('assets/img/favicons/apple-touch-icon-57x57.png')?>">
    <link rel="apple-touch-icon" sizes="60x60"   href="<?=base_url('assets/img/favicons/apple-touch-icon-60x60.png')?>">
    <link rel="apple-touch-icon" sizes="72x72"   href="<?=base_url('assets/img/favicons/apple-touch-icon-72x72.png')?>">
    <link rel="apple-touch-icon" sizes="76x76"   href="<?=base_url('assets/img/favicons/apple-touch-icon-76x76.png')?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/img/favicons/apple-touch-icon-114x114.png')?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url('assets/img/favicons/apple-touch-icon-120x120.png')?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url('assets/img/favicons/apple-touch-icon-144x144.png')?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url('assets/img/favicons/apple-touch-icon-152x152.png')?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('assets/img/favicons/apple-touch-icon-180x180.png')?>">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-32x32.png')?>" sizes="32x32">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-194x194.png')?>" sizes="194x194">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-96x96.png')?>" sizes="96x96">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/android-chrome-192x192.png')?>" sizes="192x192">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-16x16.png')?>" sizes="16x16">
    <link rel="manifest" href="<?=base_url('assets/img/favicons/manifest.json')?>">
    <meta name="msapplication-TileColor" content="#e0e0e0">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#c9c9c9">
    
    <title>CHAP - Login</title>

    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet">

    <link href="<?=base_url('assets/css/animate.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">

</head>

<body class="gray-bg">
      <noscript>
    <style type="text/css">
        #wrapper, #contenedor{display: none;}
        body {background-color: #f3f3f4;}
    </style>
<div class="middle-box text-center animated fadeInDown">
        <h1 style="margin-bottom: 20px; font-size: 160px;">Opps!</h1>
        <h3 class="font-bold">Hemos detectado un error</h3>

        <div class="error-desc">
            Hemos detectado que su navegador tiene deshabilitado Javascript.
            Por favor <strong>habilite javascript</strong> en sunavegador para poder hacer uso de CHAP.
            <div class="row">
                Para ver las instrucciones para habilitar javascript en su navegador, haga click en el boton <strong>Instrucciones</strong>,
                luego regresa a esta pagina y presiona el boton <strong>Inicio</strong>.<br>
                <a href="<?=site_url('home')?>" class="btn btn-success btn-lg"><i class="fa fa-home"></i> Inicio</a>
                <a href="http://www.enable-javascript.com/es/" class="btn btn-success btn-lg" target="_blank"><i class="fa fa-file"></i> Instrucciones</a>
            </div>
        </div>
    </div>
</noscript>
<div id="contenedor">
            <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 style="font-size: 160px" class="logo-name hidden-xs">CHAP</h1>
                <h1 style="font-size: 140px" class="logo-name visible-xs">CHAP</h1>
            </div>
            <h3>Sistema Automatizado de Control de Horarios y Asistencias de Personal</h3>
            <?php if($error=='Si'):?>
            <div class="alert alert-warning">
                Usuario o contraseña invalidos
            </div>
            <?php endif;?>
            
            <form class="m-t" role="form" action="<?=site_url('login')?>" id="form" autocomplete="off" method="POST">
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Usuario" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Contraseña" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Iniciar sesión</button>
  
            </form>
            <p class="m-t"> <small>CHAP Version 1 &copy; 2015</small> </p>
        </div>
        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">
                                Recuperar contraseña
                            </button>
                                </div>
                            <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-warning modal-icon"></i>
                                            <h4 class="modal-title">Cambio de clave</h4>
                                                  
                                        <div class="modal-body">
                                            <p>Para recuperar su contraseña de usuario debera dirigirse al supervisor del departamento de tecnología para el cambio de la misma.</p>
                                                 
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Regresar</button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
    </div>
</div>
      
    <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
    <!-- Jquery Validate -->
    <script src="<?=base_url('assets/js/plugins/validate/jquery.validate.min.js')?>"></script>
    <script>
      $(document).ready(function(){
            $("#form").validate({
              rules: {
                  username:{
                  required: true,
                  minlength: 5,
                  maxlength: 19,
                  },
                  password: {
                      required: true,
                  }
              },
              messages:{
                  username: {
                  minlength:"¡Al menos {0} son caracteres requeridos!",
                  maxlength:"No más de {0} caracteres",
                  },
              }
            });
      });
    </script>
</body>

    
        
