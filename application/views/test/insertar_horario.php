<?php
$tipo = array(                       
                       ''         => '- Seleccione -',
                       'Normal'   => 'Normal',
                       '12x12'    => '12x12',
                       'Pernocta' => 'Pernocta');
?>
<html>
    <head>
        <title>Insertar Horario</title>
    </head>
    <body>
        <h1>Insertar un nuevo horario</h1>
        <?=validation_errors()?>
        <?=form_open('horario/insertar/2')?>
            <table>
                <tr>
                    <td><?=form_label('Hora de Entrada:','hora_entrada')?></td>
                    <td><?=form_input(array('id'=>'hora_entrada','name'=>'hora_entrada','value'=>set_value('hora_entrada'),'type'=>'time'))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Hora de Salida:','hora_salida')?></td>
                    <td><?=form_input(array('id'=>'hora_salida','name'=>'hora_salida','value'=>set_value('hora_salida'),'type'=>'time'))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Descripción:','descripcion')?></td>
                    <td><?=form_textarea(array('id'=>'descripcion','name'=>'descripcion','value'=>set_value('descripcion')))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Tipo:','tipo')?></td>
                    <td><?=form_dropdown('tipo',$tipo,'')?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Insertar','Insertar')?></td>
                </tr>
            </table>
        <?=form_close()?>
    </body>
</html>