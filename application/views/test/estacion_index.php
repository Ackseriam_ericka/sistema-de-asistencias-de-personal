<html>
    <head>
        <title>Estacion Index</title>
    </head>
    <body>
        <?php if($filas != 0):?>
            <h1>Ultimos agregados</h1>
            <table border=1>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Altura</th>
                    <th>Descripcion</th>		   
                <?php foreach($filas as $fila):?>
                    <tr>
                       <td><?=$fila->estacion_id?></td>
                       <td><?=$fila->nombre?></td>
                       <td><?=$fila->altura?></td>
					   <td><?=$fila->descripcion?></td>
		       <!--<td><a href="<?=site_url('estacion/detalle/'.$fila->id)?>">Detalle</a></td>-->
		       <td><a href="<?=site_url('estacion/editar/'.$fila->estacion_id)?>">Editar</a></td>
		       <td><a href="<?=site_url('estacion/borrar/'.$fila->estacion_id)?>">Borrar</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        <?php else: ?>
            <h3>No hay estacion en la Base de datos</h3>
        <?php endif; ?>
	<h3><a href="<?=site_url('estacion/insertar')?>">Insertar</a></h3>
	<h3><a href="<?=site_url('test')?>">Index</a></h3>
    </body>
</html>
