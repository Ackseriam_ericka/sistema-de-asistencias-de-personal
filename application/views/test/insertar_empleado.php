<?php
$select_cargos = array(''=>'- Seleccione -');
foreach($cargos as $cargo)
{
    $select_cargos[$cargo->cargo_id] = $cargo->cargo;
}

$S_departamentos = array(''=>'- Seleccione -');
foreach($departamentos as $departamento)
{
    $S_departamentos[$departamento->departamento_id] = $departamento->nombre;
}

$S_estaciones = array(
  ''  => 'Seleccione',
  '1' => 'Barinitas',
  '2' => 'La Montaña',
  '3' => 'La Aguada',
  '4' => 'Loma Redona',
  '5' => 'Pico Espejo'
);
?>

<html>
    <head>
        <title>Insertar Empleado</title>
    </head>
    <body>
        <h1>Insertar un empleado</h1>
        <?=validation_errors()?>
        <?=form_open('empleado/insertar/2')?>
            <table>
                <tr>
                    <td><?=form_label('Nombre:','nombre')?></td>
                    <td><?=form_input(array('id'=>'nombre','name'=>'nombre','value'=>set_value('nombre')/*,'onfocusout'=>'getcode();'*/))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Cedula:','cedula')?></td>
                    <td><?=form_input(array('id'=>'cedula','name'=>'cedula','val><?=ue'=>set_value('cedula')))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Fecha de Nacimiento:','fecha_nac')?></td>
                    <td><?=form_input(array('id'=>'fecha_nac','name'=>'fecha_nac','value'=>set_value('fecha_nac')))?></td>
                </tr>
                <?=form_hidden('estatus','Activo')?>
                <tr>
                    <td><?=form_label('Codigo:','codigo_empl1')?></td>
                    <td id="codigo_empl"></td>
                </tr>
                <tr>
                    <td><?=form_label('Cargo:','cargo')?></td>
                    <td><?=form_dropdown('cargo_id',$select_cargos,'')?></td>
                </tr>
                <tr>
                    <td><?=form_label('Departamento:','departamento')?></td>
                    <td><?=form_dropdown('departamento_id',$S_departamentos,'')?></td>
                </tr>
                <tr>
                    <td><?=form_label('Estacion:','estacion_id')?></td>
                    <td><?=form_dropdown('estacion_id',$S_estaciones,'')?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Insertar','Insertar')?></td>
                </tr>
            </table>
        <?=form_close()?>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                $("#nombre").focus();
                $("#nombre").focusout(function(){
                    var name = $("#nombre").val();
                    $.post("<?=site_url('empleado/get_code')?>", {name: name}, function(data){
                        if (data != "") {
                            $("#codigo_empl").html(data);
                        }
                        else
                            {
                                alert("No se puede crear el codigo debe introducir al menos el Primer Nombre y el Primer Apellido");
                                $("#nombre").focus();
                            }
                    });
            });
        });
        </script>
    </body>
</html>