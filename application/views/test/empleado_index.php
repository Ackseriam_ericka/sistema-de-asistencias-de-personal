<html>
    <head>
        <title>Empleado Index</title>
    </head>
    <body>
        <?php if($filas != 0):?>
            <h1>Ultimos agregados</h1>
            <table border=1>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Cédula</th>
		    <th>Codigo del Empleado</th>
                    <th>Estatus</th>
		    <th>Tipo de contrato</th>
		    <th colspan=3>Acciones</th>
                </tr>
                <?php foreach($filas as $fila):?>
                    <tr>
                       <td><?=$fila->empleado_id?></td>
                       <td><?=$fila->nombre?></td>
		       <td><?=$fila->cedula?></td>
		       <td><?=$fila->codigo_empl?></td>
                       <td><?=$fila->estatus?></td>
		       <td><?=$fila->tipo_contrato?></td>
		       <td><a href="<?=site_url('empleado/detalle/'.$fila->empleado_id)?>">Detalle</a></td>
		       <td><a href="<?=site_url('empleado/editar/'.$fila->empleado_id)?>">Editar</a></td>
		       <td><a href="<?=site_url('empleado/borrar/'.$fila->empleado_id)?>">Borrar</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        <?php else: ?>
            <h3>No hay empleados en la Base de datos</h3>
        <?php endif; ?>
	<h3><a href="<?=site_url('empleado/insertar')?>">Insertar</a></h3>
	<h3><a href="<?=site_url('test')?>">Index</a></h3>
    </body>
</html>
