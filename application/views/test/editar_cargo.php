<html>
    <head>
        <title>Editar Cargo</title>
    </head>
    <body>
        <h1>Editar Cargo</h1>
        <?=validation_errors()?>
        <?=form_open('cargo/editar/'.$fila[0]->cargo_id.'/2')?>

            <table>
                <tr>
                    <td><?=form_label('Cargo:','cargo')?></td>
                    <td><?=form_input(array('id'=>'cargo','name'=>'cargo','value'=>(set_value('cargo')!='' ? set_value('cargo') : $fila[0]->cargo)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Descripcion:','descripcion')?></td>
                    <td><?=form_textarea(array('id'=>'descripcion','name'=>'descripcion','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?></td>
                </tr>
                <?=form_hidden('cargo_id',$fila[0]->cargo_id)?>
                <tr>
                    <td><?=form_submit('Insertar','Modificar')?></td>
                </tr>
            </table>
        <?=form_close()?>
       
    </body>
</html>