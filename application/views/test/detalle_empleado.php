<html>
    <head>
        <title>Detalle empleado</title>
    </head>
    <body>
        <?php if ($fila!=0):?>
        <h3>Información del trabajador</h3>
        <table>
            <tr>
                <th>Codigo del Empleado</th>
                <td><?=$fila[0]->codigo_empl?></td>
            </tr>
            <tr>
                <th>Nombre Completo</th>
                <td><?=$fila[0]->nombre?></td>
            </tr>
            <tr>
                 <th>Cédula</th>
                 <td><?=$fila[0]->cedula?></td>
            </tr>
            <tr>
                <th>Fecha de Nacimiento</th>
                <td><?=$fila[0]->fecha_nac?></td>
            </tr>
            <tr>
                <th>Estatus</th>
                <td><?=$fila[0]->estatus?></td>
            </tr>
            <tr>
                <th>Cargo</th>
                <td><?=$fila[0]->cargo?></td>
            </tr>
            <tr>
                <th>Adjunto al departamento</th>
                <td><?=$fila[0]->dep_nombre?></td>
            </tr>
            <tr>
                <th>Trabaja en la estacion</th>
                <td><?=$fila[0]->estacion?></td>
            </tr>
            </table>
            <h3><a href="<?=site_url('empleado/editar/'.$fila[0]->empleado_id).'/1'?>">Editar informacion</a></h3>
            <?php else :?>
                <h3>No se pudo encontrar la informacion del trabajor</h3>
            <?php endif;?>
            <h3><a href="<?=site_url('empleado')?>">Lista de empleados</a></h3>
    </body>
</html>
