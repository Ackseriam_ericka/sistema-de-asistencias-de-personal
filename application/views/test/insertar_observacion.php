<?php
$select_empleados = array(''=>'- Seleccione -');
    foreach($empleados as $empleado)
    {
        $select_empleados[$empleado->empleado_id] = $empleado->nombre;
    }

?>

<?php
$tipo = array(                       
                        ''         => '- Seleccione -',
                       'Falta Justificada'      => 'Falta Justificada',
                       'Falta Injustificada'    => 'Falta Injustificada',
                       'Observación'            => 'Observación',
                       'Salida antes de tiempo' => 'Salida antes de tiempo');
?>

<?php
$tipo2 = array(                       
                       ''         => '- Seleccione -',
                       'Si'=> 'Si',
                       'No'=> 'No');
?>


<html>
    <head>
        <title>Insertar Observacion</title>
    </head>
    <body>
        <h1>Insertar una observacion</h1>
        <?=validation_errors()?>
        <?=form_open('observacion/insertar/2')?>
            <table>
                <tr>
                    <td><?=form_label('Descripcion:','descripcion')?></td>
                    <td><?=form_textarea(array('id'=>'descripcion','name'=>'descripcion','value'=>set_value('descripcion')))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Tipo:','tipo')?></td>
                    <td><?=form_dropdown('tipo',$tipo,'')?></td>

                </tr>
                <tr>
                    <td><?=form_label('Asistencia:','asistencia')?></td>
                    <td><?=form_dropdown('asistencia',$tipo2,'')?></td>
                </tr>
                <tr>
                    <td><?=form_label('Empleado:','empleado_id')?></td>
                    <td><?=form_dropdown('empleado_id',$select_empleados,'')?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Insertar','Insertar')?></td>
                </tr>
            </table>
        <?=form_close()?>

    </body>
</html>