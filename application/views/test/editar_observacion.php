<?php
$select_empleados = array(''=>'- Seleccione -');
    foreach($empleados as $empleado)
    {
        $select_empleados[$empleado->empleado_id] = $empleado->nombre;
    }

?>
<?php
$tipo = array(                       
                       ''         => '- Seleccione -',
                       'Falta '   => 'Falta',
                       'Retraso'    => 'Retraso');
?>
<?php
$tipo2 = array(                       
                       ''         => '- Seleccione -',
                       'Asistio '   => 'Asistio',
                       'Inasistido'    => 'Inasistido');
?>
<html>
    <head>
        <title>Editar Observacion</title>
    </head>
    <body>
        <h1>Editar Observacion</h1>
        <?=validation_errors()?>
        <?=form_open('observacion/editar/'.$fila[0]->codigo_justi.'/2')?>
            <table>
                <tr>
                    <td><?=form_label('Descripción:','descripcion')?></td>
                    <td><?=form_input(array('codigo_justi'=>'descripcion','name'=>'descripcion','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?></td>
                </tr>
                 <tr>
                    <td><?=form_label('Tipo:','tipo')?></td>
                    <td><?=form_dropdown('tipo',$tipo,(set_value('tipo')!='' ? set_value('tipo') : $fila[0]->tipo))?></td>
                </tr>
                  <tr>
                    <td><?=form_label('Asistencia:','asistencia')?></td>
                    <td><?=form_dropdown('asistencia',$tipo2,(set_value('asistencia')!='' ? set_value('asistencia') : $fila[0]->asistencia))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Empleado:','empleado')?></td>
                    <td><?=form_dropdown('empleado_id',$select_empleados,(set_value('empleado_id')!='' ? set_value('empleado_id') : $fila[0]->empleado_id))?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Actualizar','Actualizar')?></td>
                </tr>
                <?=form_hidden('codigo_justi',$fila[0]->codigo_justi)?>
            </table>
        <?=form_close()?>
    </body>
</html>