<html>
    <head>
        <title>Detalle Estacion</title>
    </head>
    <body>
        <?php if ($fila!=0):?>
        <h3>Información de la estacion</h3>
        <table>
            <tr>
                <th>Nombre de la estacion</th>
                <td><?=$fila[0]->nombre?></td>
            </tr>
            <tr>
                <th>Altura de la estacion</th>
                <td><?=$fila[0]->altura?></td>
            </tr>
            <tr>
                <th>Descripcion de la estacion</th>
                <td><?=$fila[0]->descripcion?></td>
            </tr>
           
            </table>
            <h3><a href="<?=site_url('estacion/editar/'.$fila[0]->estacion_id).'/1'?>">Editar informacion</a></h3>
            <?php else :?>
                <h3>No se pudo encontrar la informacion de la estacion</h3>
            <?php endif;?>
            <h3><a href="<?=site_url('estacion')?>">Lista de las estaciones</a></h3>
    </body>
</html>
