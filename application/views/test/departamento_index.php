<html>
    <head>
        <title>Departamento Index</title>
    </head>
    <body>
        <?php if($filas != 0):?>
            <h1>Ultimos agregados</h1>
            <table border=1>
                <tr>
                    <th>Id</th>
                    <th>Nombre del departamento</th>
                    <th>Supervisor</th>
                    <th>Coordinador</th>
                    <th>Descripción</th>
		    <th colspan=3>Acciones</th>
                </tr>
                <?php foreach($filas as $fila):?>
                    <tr>
                       <td><?=$fila->departamento_id?></td>
                       <td><?=$fila->nombre?></td>
                       <td><?=$fila->supervisor?></td>
		               <td><?=$fila->coordinador?></td>
                       <td><?=$fila->descripcion?></td>
              
		       <td><a href="<?=site_url('departamento/detalle/'.$fila->departamento_id)?>">Detalle</a></td>
		       <td><a href="<?=site_url('departamento/editar/'.$fila->departamento_id)?>">Editar</a></td>
		       <td><a href="<?=site_url('departamento/borrar/'.$fila->departamento_id)?>">Borrar</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        <?php else: ?>
            <h3>No hay departamentos en la Base de datos</h3>
        <?php endif; ?>
	<h3><a href="<?=site_url('departamento/insertar')?>">Insertar</a></h3>
	<h3><a href="<?=site_url('test')?>">Index</a></h3>
    </body>
</html>
