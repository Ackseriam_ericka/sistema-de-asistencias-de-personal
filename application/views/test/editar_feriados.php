<html>
    <head>
        <title>Editar editar feriados</title>
    </head>
    <body>
        <h1>Editar feriados</h1>
        <?=validation_errors()?>
        <?=form_open('feriados/editar/'.$fila[0]->feriado_id.'/2')?>
            <table>
                <tr>
                    <td><?=form_label('Dia:','dia')?></td>
                    <td><?=form_input(array('feriado_id'=>'dia','name'=>'dia','value'=>(set_value('dia')!='' ? set_value('dia') : $fila[0]->dia)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Mes','mes')?></td>
                    <td><?=form_input(array('feriado_id'=>'mes','name'=>'mes','value'=>(set_value('mes')!='' ? set_value('mes') : $fila[0]->mes)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Ano:','ano')?></td>
                    <td><?=form_input(array('feriado_id'=>'ano','name'=>'ano','value'=>(set_value('ano')!='' ? set_value('ano') : $fila[0]->ano)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Descripción:','descripcion')?></td>
                    <td><?=form_textarea(array('feriado_id'=>'descripcion','name'=>'descripcion','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Actualizar','Actualizar')?></td>
                </tr>
                <?=form_hidden('feriado_id',$fila[0]->feriado_id)?>
            </table>
        <?=form_close()?>
    </body>
</html>
