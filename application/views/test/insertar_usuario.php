<?php
$selec_empleados = array(''=>'- Seleccione -');
    foreach($empleados as $empleado)
    {
        $selec_empleados[$empleado->empleado_id] = $empleado->nombre;
    }

?>

<html>
    <head>
        <title>Insertar usuario</title>
        <style>
            .error{
                color: red;
            }
        </style>
    </head>
    <body>
        <h1>Insertar un usuario</h1>
        <span class="error"><?=validation_errors()?></span>
        <?=form_open('usuario/insertar/2')?>
            <table>
                <tr>
                    <td><?=form_label('Usuario:','usuario')?></td>
                    <td><?=form_input(array('id'=>'usuario','name'=>'usuario','value'=>set_value('usuario')/*,'onfocusout'=>'getcode();'*/))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Contraseña:','contrasena')?></td>
                    <td><?=form_password(array('id'=>'contrasena','name'=>'contrasena'))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Repita contraseña:','confcontrasena')?></td>
                    <td><?=form_password(array('id'=>'confcontrasena','name'=>'confcontrasena'))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Email:','email')?></td>
                    <td><?=form_input(array('id'=>'email','name'=>'email','value'=>set_value('email')))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Empleado:','empleado_id')?></td>
                    <td><?=form_dropdown('empleado_id',$selec_empleados,'')?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Insertar','Insertar')?></td>
                </tr>
            </table>
        <?=form_close()?>
    </body>
</html>