<html>
    <head>
        <title>Insertar cargo</title>
    </head>
    <body>
        <h1>Insertar un empleado</h1>
        <?=validation_errors()?>
        <?=form_open('cargo/insertar/2')?>
            <table>
                <tr>
                    <td><?=form_label('Cargo:','cargo')?></td>
                    <td><?=form_input(array('id'=>'cargo','name'=>'cargo','value'=>set_value('cargo')/*,'onfocusout'=>'getcode();'*/))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Descripcion:','descripcion')?></td>
                    <td><?=form_input(array('id'=>'descripcion','name'=>'descripcion','value'=>set_value('descripcion')))?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Insertar','Insertar')?></td>
                </tr>
            </table>
        <?=form_close()?>
    </body>
</html>