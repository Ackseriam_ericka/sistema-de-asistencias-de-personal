<?php
$S_empleados = array(''=>'- Seleccione -');
foreach($empleados as $empleado)
{
    $S_empleados[$empleado->nombre] = $empleado->nombre;
}
?>
      <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Departamentos</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('departamento')?>">Departamentos</a>
                        </li>
                        <li class="active">
                            <strong>Insertar departamento</strong>
                        </li>
                    </ol>
                </div>

          </div>
               <div class="wrapper wrapper-content">
                
                <div class="row">
                   <div class="col-lg-8 col-lg-offset-2">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3>Insertar Departamento</h3>
                            </div>

                                <div class="panel-body">
                                    <?=validation_errors()?>
                                    <?=form_open('departamento/insertar/2', array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                                        <div class="form-group">
                                             <?=form_label('Nombre:','nombre')?>
                                             <?=form_input(array('id'=>'nombre','name'=>'nombre','class'=>'form-control','value'=>set_value('nombre'),'required'=>'required'))?>
                                        </div>
                                        <div class="form-group">
                                             <?=form_label('Responsable:','responsable')?>
                                             <?=form_dropdown('responsable',$S_empleados,(set_value('departamento_id')),'class="chosen-select form-control" id="responsable" required')?>
                                        </div>
                                        <div class="form-group">
                                            <?=form_label('Descripcion:','descripcion')?>
                                            <?=form_textarea(array('id'=>'descripcion','name'=>'descripcion','class'=>'form-control','value'=>set_value('descripcion'), 'required'=>'required'))?>
                                        </div>
                                            
                                        <div class="row">
                                            <div class="col-lg-6 col-lg-offset-3">
                                                <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                                <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                                            </div>
                                        </div>
                                            
                                        <?=form_close()?>
                                </div>
                        </div>
                    </div>
             </div>