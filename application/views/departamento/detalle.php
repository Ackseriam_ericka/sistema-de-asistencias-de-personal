<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Departamentos</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('departamento')?>">Departamentos</a>
                        </li>
                        <li class="active">
                            <strong>Detalle del departamento</strong>
                        </li>
                    </ol>
                </div>

          </div>
            <div class="wrapper wrapper-content">
                <?php if($fila[0]->responsable == '' || $fila[0]->descripcion == ''):?>
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 alert alert-danger">
                        <span class="fa-2x"><i class="fa fa-warning"></i> Atencion.</span>
                        <p>
                            Hemos detectado que este depertamento, coordinación u oficina no tiene un reposable o falta información sobre el mismo
                        asegurese de completar esta información puesto que es necesaria en algunos procesos del sistema.
                        </p>
                    </div>
                </div>
                <?php endif;?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                         <?php if($fila!=0):?>
                            <div class="ibox-title">
                                <div class="m-b-md">
                                    <a href="<?=site_url('departamento/editar/'.$fila[0]->departamento_id.'/1')?>" class="btn btn-white btn-sm pull-right">Editar información</a>
                                    <h3><strong>Departamento</strong>: <?=$fila[0]->nombre?></h3>
                                </div>
                             </div>  
                             <div class="ibox-content">
                                <div class="row">
                                <div class="col-lg-12">
                                    <dl class="dl-horizontal">
                                        <dt>Responsable:</dt> <dd><?=($fila[0]->responsable)=='' ? 'Debe seleccionar un responsable' : $fila[0]->responsable ?></dd>
                                        <dt>Descripcion:</dt><dd><?=($fila[0]->descripcion)=='' ? 'Debe crear una descripción para el departamento o coordinación' : $fila[0]->descripcion ?></dd>  
                                    </dl>
                                </div>
                                </div>
                                <!--<div class="row">
                                    <div class="col-lg-12">
                                        <dl class="dl-horizontal">
                                            <dt>Completed:</dt>
                                            <dd>
                                                <div class="progress progress-striped active m-b-sm">
                                                    <div style="width: 60%;" class="progress-bar"></div>
                                                </div>
                                                <small>Project completed in <strong>60%</strong>. Remaining close the project, sign a contract and invoice.</small>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>-->
                           </div>
                            <?php else :?>
                                    <div class="alert alert-info">
                                        <h3>Información</h3>
                                        <span>No existen Justificaciones registrados</span>
                                    </div>
                            <?php endif;?>
                        </div>
                    </div>
               </div>
                <div class="row">
                    <div class="col-lg-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3>Empleados</h3>
                        </div>
                        <div class="panel-body">
                            <?php if($empleados != 0 ):?>
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th style="width:10%">Codigo del Empleado</th>
                                            <th>Nombre</th>
                                            <th>Cédula</th>
                                            <th style="width:15%">Fecha de ingreso</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($empleados as $empleado):?>
                                        <tr class="odd gradeX">
                                            <td><?=$empleado->codigo_empl?></td>
                                            <td><?=$empleado->nombre?></td>
                                            <td><?=$empleado->cedula?></td>
                                            <td><?=$empleado->fecha_ingreso?></td>
                                            <td>
                                                <div class="btn-group tooltip-demo">
                                                    <a href="<?=site_url('empleado/detalle/'.$empleado->empleado_id)?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Detalles"><i class="fa fa-search"></i></a>
                                                    <?php $nivel = $this->session->userdata('usuario')['rol'];
                                                    if ($nivel == 1 ) :?>
                                                    <a href="<?=site_url('empleado/editar/'.$empleado->empleado_id.'/1')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Editar información"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?=site_url('asignacion/insertar/1/'.$empleado->empleado_id)?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Crear asignación"><i class="fa fa-calendar-o"></i></a>
                                                    <?php elseif($nivel == 2):?>
                                                    <a href="<?=site_url('asignacion/insertar/1/'.$empleado->empleado_id)?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Crear asignación"><i class="fa fa-calendar-o"></i></a>
                                                    <?php endif;?>
                                                    <!--<a href="<?=site_url('empleado/borrar/'.$empleado->empleado_id)?>">Borrar</a>-->
                                                </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                    <!--<tfoot>
                                        <tr>
                                            <th>Rendering engine</th>
                                            <th>Browser</th>
                                            <th>Platform(s)</th>
                                            <th>Engine version</th>
                                            <th>CSS grade</th>
                                        </tr>
                                    </tfoot>-->
                                </table>
                            <?php else :?>
                                <div class="alert alert-info">
                                    <h3>Información</h3>
                                    <span>No existen empleados registrados</span>
                                </div>
                            <?php endif;?>
                        </div>
                        <div class="panel-footer">
                            <?=form_open('reporte/relacionN',array('target'=>'_blank'))?>
                                <?=form_hidden('departamento_id',$fila[0]->departamento_id)?>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-print"></i> Imprimir listado de personal
                                </button>
                            <?=form_close()?>
                            <!--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal4">
                                <i class="fa fa-print"></i> Imprimir Relación Nomina
                            </button>
                            <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated fadeIn">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-print modal-icon"></i>
                                            <h4 class="modal-title">Imprimir relación nomina</h4>
                                            <small>Selecciona la coordinación o departamento del cual deseas obtener a relación nomina</small>
                                        </div>
                                        <div class="modal-body">
                                            <?=form_open('reporte/relacionN')?>
                                                <div class="form-group">
                                                    <?=form_label('Departamento:','departamento')?></td>
                                                    <span><?=$fila[0]->nombre?></span>
                                                    <?=form_hidden('departamento_id',$fila[0]->departamento_id)?>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary">Imprimir</button>
                                        </div>
                                        <?=form_close()?>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
           </div>
  