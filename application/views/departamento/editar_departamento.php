
<html>
    <head>
        <title>Editar Departamento</title>
    </head>
    <body>
        <h1>Editar Departamento</h1>
        <?=validation_errors()?>
        <?=form_open('departamento/editar/'.$fila[0]->departamento_id.'/2')?>
            <table>
                <tr>
                    <td><?=form_label('Nombre:','nombre')?></td>
                    <td><?=form_input(array('departamento_id'=>'nombre','name'=>'nombre','value'=>(set_value('nombre')!='' ? set_value('nombre') : $fila[0]->nombre)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Supervisor','supervisor')?></td>
                    <td><?=form_textarea(array('departamento_id'=>'supervisor','name'=>'supervisor','value'=>(set_value('supervisor')!='' ? set_value('supervisor') : $fila[0]->supervisor)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Coordinador:','coordinador')?></td>
                    <td><?=form_textarea(array('departamento_id'=>'coordinador','name'=>'descripcion','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Descripción:','descripcion')?></td>
                    <td><?=form_textarea(array('departamento_id'=>'descripcion','name'=>'descripcion','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Actualizar','Actualizar')?></td>
                </tr>
                <?=form_hidden('departamento_id',$fila[0]->departamento_id)?>
            </table>
        <?=form_close()?>
    </body>
</html>