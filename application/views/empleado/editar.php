<?php
$select_cargos = array(''=>'- Seleccione -');
foreach($cargos as $cargo)
{
    $select_cargos[$cargo->cargo_id] = $cargo->cargo;
}

$S_departamentos = array(''=>'- Seleccione -');
foreach($departamentos as $departamento)
{
    $S_departamentos[$departamento->departamento_id] = $departamento->nombre;
}

$S_estaciones = array(
  ''  => '- Seleccione -',
  '1' => 'Barinitas',
  '2' => 'La Montaña',
  '3' => 'La Aguada',
  '4' => 'Loma Redona',
  '5' => 'Pico Espejo'
);
?>

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Empleados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('empleado')?>">Empleados</a>
                        </li>
                        <li class="active">
                            <strong>Editar empleado:  </strong> <?=$fila[0]->nombre?>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                
                <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>Modificar Datos</h3>
                    </div>
                    
                    <div class="panel-body">
                    <?=validation_errors('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');?>
                    <?php if($error_subida):?>
                    <div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?=$error_subida?>
                    </div>
                    <?php endif;?>
                    <?=form_open_multipart('empleado/editar/'.$fila[0]->empleado_id.'/2',array('role'=>'form','id'=>'form'))?>
                        <div class="form-group">
                            <?=form_label('Nombre:','nombreE')?>
                            <?=form_input(array('id'=>'nombreE','name'=>'nombre','class'=>'form-control','value'=>(set_value('nombre')!='' ? set_value('nombre') : $fila[0]->nombre),'required'=>'required','onKeyUp'=>'this.value=this.value.toUpperCase();'))?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Cedula:','cedula')?></td>
                            <?=form_input(array('id'=>'cedula','name'=>'cedula','class'=>'form-control','value'=>(set_value('cedula')!='' ? set_value('cedula') : $fila[0]->cedula),'required'=>'required'))?>
                            <!--<span class="help-block">El formato para la cédula es xx.xxx.xxx</span>-->
                        </div>
                        <div class="form-group" id="data_1">
                                <?=form_label('Fecha de Nacimiento:','fecha_nac')?>
                                <div class="input-group date " >
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="fecha_nac" type="text" required class="form-control" value="<?=(set_value('fecha_nac')!='' ? set_value('fecha_nac') : $fila[0]->fecha_nac)?>">
                                </div>
                            </div>
                        <div class="form-group">
                            <?=form_label('Codigo:','codigo_empl')?>
                            <div id="codigo_empl"><?=form_input(array('id'=>'codigo_empl','name'=>'codigo_empl','class'=>'form-control','value'=>(set_value('codigo_empl')!='' ? set_value('codigo_empl') : $fila[0]->codigo_empl),'readonly'=>'readonly'))?></div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Tipo de contrato','contrato')?>
                            <br>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <?php if ($fila[0]->tipo_contrato=='Fijo'):?>
                                <label class="btn btn-primary btn-outline active">
                                  <input type="radio" name="contrato" id="option2" checked autocomplete="off" value="Fijo"><i class="glyphicon glyphicon-ok"></i> Fijo
                                </label>
                                <label class="btn btn-default btn-outline">
                                  <input type="radio" name="contrato" id="option3" autocomplete="off" value="Contratado"><i class="glyphicon glyphicon-repeat"></i> Contratado
                                </label>
                                <?php elseif ($fila[0]->tipo_contrato=='Contratado'):?>
                                <label class="btn btn-primary btn-outline ">
                                  <input type="radio" name="contrato" id="option2" autocomplete="off" value="Fijo"><i class="glyphicon glyphicon-ok"></i> Fijo
                                </label>
                                <label class="btn btn-default btn-outline active">
                                  <input type="radio" name="contrato" id="option3" checked autocomplete="off" value="Contratado"><i class="glyphicon glyphicon-repeat"></i> Contratado
                                </label>
                                <?php else:?>
                                <label class="btn btn-primary btn-outline ">
                                  <input type="radio" name="contrato" id="option2" autocomplete="off" value="Fijo"><i class="glyphicon glyphicon-ok"></i> Fijo
                                </label>
                                <label class="btn btn-default btn-outline">
                                  <input type="radio" name="contrato" id="option3" autocomplete="off" value="Contratado"><i class="glyphicon glyphicon-repeat"></i> Contratado
                                </label>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Estatus','estatus')?>
                            <br>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <?php if ($fila[0]->estatus=='Activo'):?>
                                <label class="btn btn-primary btn-outline active">
                                  <input type="radio" name="estatus"  checked autocomplete="off" value="Activo"><i class="glyphicon glyphicon-ok"></i> Activo
                                </label>
                                <label class="btn btn-danger btn-outline">
                                  <input type="radio" name="estatus"  autocomplete="off" value="Inactivo"><i class="glyphicon glyphicon-remove"></i> Inactivo
                                </label>
                                <?php else:?>
                                <label class="btn btn-primary btn-outline">
                                  <input type="radio" name="estatus" id="option2" autocomplete="off" value="Activo"><i class="glyphicon glyphicon-ok"></i> Activo
                                </label>
                                <label class="btn btn-danger btn-outline active">
                                  <input type="radio" name="estatus" id="option3" checked autocomplete="off" value="Inactivo"><i class="glyphicon glyphicon-remove"></i> Inactivo
                                </label>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Cargo:','cargo')?></td>
                            <?=form_dropdown('cargo_id',$select_cargos,(set_value('cargo_id')!='' ? set_value('cargo_id') : $fila[0]->cargo_id),'class="chosen-select form-control" id="cargo" required="required"')?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Departamento:','departamento')?></td>
                            <?=form_dropdown('departamento_id',$S_departamentos,(set_value('departamento_id')!='' ? set_value('departamento_id') : $fila[0]->departamento_id),'class="chosen-select form-control" id="departamento" required')?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Estacion:','estacion')?></td>
                            <?=form_dropdown('estacion_id',$S_estaciones,(set_value('estacion_id')!='' ? set_value('estacion_id') : $fila[0]->estacion_id),'class="form-control" id="estacion" required="required"')?>
                        </div>
                        <!--<div class="form-group">
                            <?=form_label('Fotografia:','imagen')?>
                            <input type="file" name="userfile" id="imagen">
                        </div>-->
                        <div id="img">
                            
                        </div>
                        <input type="hidden" id="empleado_id" name="empleado_id" value="<?=$fila[0]->empleado_id?>"/>
                        
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
                                <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                            </div>
                        </div>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>
            </div>
