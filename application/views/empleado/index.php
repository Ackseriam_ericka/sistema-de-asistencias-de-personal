<?php
$S_departamentos = array('0'=>'- Relación Nomina Completa -');
foreach($departamentos as $departamento)
{
    $S_departamentos[$departamento->departamento_id] = $departamento->nombre;
}
?>
            <div class="row wrapper border-bottom white-bg page-heading">   <!--migas de pan -->
                <div class="col-sm-4">
                    <h2>Empleados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li class="active">
                            <strong>Empleados</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3>Empleados</h3>
                        </div>
                        <div class="panel-body">
                            <?php if($filas != 0 ):?>
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th style="width:10%">Codigo del Empleado</th>
                                            <th>Nombre</th>
                                            <th>Cédula</th>
                                            <th style="width:15%">Fecha de ingreso</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($filas as $fila):?>
                                        <tr class="odd gradeX">
                                            <td><?=$fila->codigo_empl?></td>
                                            <td><?=$fila->nombre?></td>
                                            <td><?=$fila->cedula?></td>
                                            <td><?=$fila->fecha_ingreso?></td>
                                            <td>
                                                <div class="btn-group tooltip-demo">
                                                    <a href="<?=site_url('empleado/detalle/'.$fila->empleado_id)?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Detalles"><i class="fa fa-search"></i></a>
                                                    <?php $nivel = $this->session->userdata('usuario')['rol'];
                                                    if ($nivel == 1 ) :?>
                                                    <a href="<?=site_url('empleado/editar/'.$fila->empleado_id.'/1')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Editar información"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?=site_url('asignacion/insertar/1/'.$fila->empleado_id)?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Crear asignación"><i class="fa fa-calendar-o"></i></a>
                                                    <?php elseif($nivel == 2):?>
                                                    <a href="<?=site_url('asignacion/insertar/1/'.$fila->empleado_id)?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Crear asignación"><i class="fa fa-calendar-o"></i></a>
                                                    <?php endif;?>
                                                    <!--<a href="<?=site_url('empleado/borrar/'.$fila->empleado_id)?>">Borrar</a>-->
                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                    <!--<tfoot>
                                        <tr>
                                            <th>Rendering engine</th>
                                            <th>Browser</th>
                                            <th>Platform(s)</th>
                                            <th>Engine version</th>
                                            <th>CSS grade</th>
                                        </tr>
                                    </tfoot>-->
                                </table>
                            <?php else :?>
                                <div class="alert alert-info">
                                    <h3>Información</h3>
                                    <span>No existen empleados registrados</span>
                                </div>
                            <?php endif;?>
                        </div>
                        <div class="panel-footer">
                            <?php if($this->session->userdata('usuario')['rol']==1):?>
                            <a href="<?=site_url('empleado/insertar')?>" class="btn btn-success"><i class="fa fa-plus"></i> Agregar empleados</a>

                            <?php endif;?>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal4">
                                <i class="fa fa-print"></i> Imprimir Relación Nomina
                            </button>
                            <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated fadeIn">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-print modal-icon"></i>
                                            <h4 class="modal-title">Imprimir relación nomina</h4>
                                            <small>Selecciona la coordinación o departamento del cual deseas obtener a relación nomina</small>
                                        </div>
                                        <div class="modal-body">
                                            <?=form_open('reporte/relacionN',array('target'=>'_blank'))?>
                                                <div class="form-group">
                                                    <?=form_label('Departamento:','departamento')?></td>
                                                    <?=form_dropdown('departamento_id',$S_departamentos,set_value('departamento_id'),'class="chosen-select form-control" id="departamento" required="required" tabindex="2"')?>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary">Imprimir</button>
                                        </div>
                                        <?=form_close()?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>