            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Bienvenido</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Inicio</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                <div class="text-center animated fadeInRightBig">
                    <h2 class="font-bold">Bienvenido CHAP, <?=$this->session->userdata('usuario')['usuario']?></h2>
                    <div class="row">
                        <!--<div class="col-lg-6">
                            <p>El sistema de control de asistencias personal diseñado y desarrollado por los estudiantes de la UPTM Kléber Ramírez</p>
                            <img src="<?=base_url('assets/img/UPTMlogo.png')?>">
                            
                        </div>
                        <div class="col-lg-6">
                            <p>Con el completo apoyo y colobaración de las coordinaciondes: Talento Humano y Tecnología y Sistemas de la Información de Mukumbarí Sistema Teleférico de Mérida</p>
                            <img src="<?=base_url('assets/img/Mlogo.png')?>" class="col-lg-6">
                        </div>-->
                        <div class="col-lg-8 col-lg-offset-2">
                            <p>CHAP, Control de Horarios y Asistencias de personal permite al personal de la empresa
                            gestionar todo lo relacionado a los horarios de trabajo de los trabajores, así mismo esta herramienta
                            se encarga de automatizar el proceso de Entrada/Salida del trabajor. Por ultimo este software integra
                            funciones que le daran acceeso a prenominas y otros archivos importantes.</p>
                        </div>
                    </div>
                    <!--<div class="error-desc">
                        Puedes crear cualquier diseño de cuadriculas que quieras. Y cualquier diseño que imagines :)
                        Revisa el tablero principal y otras paginas. Se utilizan muchos diseños diferentes
                        <!--You can create here any grid layout you want. And any variation layout you imagine:) Check out
                        main dashboard and other site. It use many different layout.
                        <br/><a href="<?=site_url('home')?>" class="btn btn-primary m-t">Inicio</a>
                    </div>-->
                </div>
            </div>
