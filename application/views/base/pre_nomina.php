<?php
$meses = array(
  ''  => '- Mes -',
  '01' => 'Enero',
  '02' => 'Febrero',
  '03' => 'Marzo',
  '04' => 'Abril',
  '05' => 'Mayo',
  '06' => 'Junio',
  '07' => 'Julio',
  '08' => 'Agosto',
  '09' => 'Septiembre',
  '10' => 'Octubre',
  '11' => 'Noviembre',
  '12' => 'Diciembre'  
);

$mesac = date('m');
$ano = date('Y');
$S_ano = array(
  $ano => $ano,
  $ano-1 => $ano-1,
);

$S_departamentos = array(''=>'- Seleccione -');
foreach($departamentos as $departamento)
{
    $S_departamentos[$departamento->departamento_id] = $departamento->nombre;
}
?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Reportes</h2>
                    <ol class="breadcrumb">
                        
                        <li class="active">
                            <strong>Reportes</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h4>Pre-Nomina</h4>
                            </div>
                            <div class="ibox-content">
                                <?php if($error):?>
                                <div class="alert alert-danger">
                                    <?=$error?>
                                </div>
                                <?php endif;?>
                                <?=form_open('reporte/prenomina2',array('role'=>'form','id'=>'form','target'=>'_blank'))?>
                                <div class="form-group">
                                    <?=form_label('Departamento','depatamento_id')?>
                                     <?=form_dropdown('departamento_id',$S_departamentos,(set_value('departamento_id')),'class="chosen-select form-control" id="departamento" required')?>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                        <?=form_label('Mes','mes')?>
                                        <?=form_dropdown('mes',$meses,$mesac,'class="form-control" id="mes" required')?>    
                                        </div>
                                        <div class="col-lg-6">
                                            <?=form_label('Año','ano')?>
                                            <?=form_dropdown('ano',$S_ano,'','class="form-control" id="ano" required')?>    
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?=form_label('Quincena','quincena')?>
                                    <select id="quincena" class="form-control" required>
                                        <option value="">- Seleccione -</option>
                                        <option value="1">Primera quincena</option>
                                        <option value="2">Segunda quincena</option>
                                    </select>
                                </div>
                                <input type="hidden" name="fecha_ini" id="fecha_ini">
                                <input type="hidden" name="fecha_fin" id="fecha_fin">
                                <div class="row">
                                    <div class="col-lg-6 col-lg-offset-3">
                                        <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Generar</button>
                                        <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                                    </div>
                                </div>
                                <?=form_close()?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>