<!DOCTYPE html>
<html>
      
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="57x57"   href="<?=base_url('assets/img/favicons/apple-touch-icon-57x57.png')?>">
    <link rel="apple-touch-icon" sizes="60x60"   href="<?=base_url('assets/img/favicons/apple-touch-icon-60x60.png')?>">
    <link rel="apple-touch-icon" sizes="72x72"   href="<?=base_url('assets/img/favicons/apple-touch-icon-72x72.png')?>">
    <link rel="apple-touch-icon" sizes="76x76"   href="<?=base_url('assets/img/favicons/apple-touch-icon-76x76.png')?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/img/favicons/apple-touch-icon-114x114.png')?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url('assets/img/favicons/apple-touch-icon-120x120.png')?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url('assets/img/favicons/apple-touch-icon-144x144.png')?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url('assets/img/favicons/apple-touch-icon-152x152.png')?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('assets/img/favicons/apple-touch-icon-180x180.png')?>">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-32x32.png')?>" sizes="32x32">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-194x194.png')?>" sizes="194x194">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-96x96.png')?>" sizes="96x96">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/android-chrome-192x192.png')?>" sizes="192x192">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-16x16.png')?>" sizes="16x16">
    <link rel="manifest" href="<?=base_url('assets/img/favicons/manifest.json')?>">
    <meta name="msapplication-TileColor" content="#e0e0e0">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#c9c9c9">
    
    <title><?php echo $titulo?></title>

    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet">
    
    <?php if($controlador=='Listar'):?>
    <!-- Data Tables -->
    <link href="<?=base_url('assets/css/plugins/dataTables/dataTables.bootstrap.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/plugins/dataTables/dataTables.responsive.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/plugins/dataTables/dataTables.tableTools.min.css')?>" rel="stylesheet">

    <?php elseif($controlador=='Insertar' || $controlador=='Insertar Empleado' || $controlador=='Editar' || $controlador=='Editar Empleado') :?>
    <link href="<?=base_url('assets/css/plugins/datapicker/datepicker3.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/plugins/jasny/jasny-bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/plugins/chosen/chosen.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')?>" rel="stylesheet">
    <!--<link href="<?=base_url('assets/css/plugins/summernote/summernote.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/plugins/summernote/summernote-bs3.css')?>" rel="stylesheet">-->
    
    <?php elseif($controlador=='inicio'):?>
    <link href="<?=base_url('assets/css/plugins/toastr/toastr.min.css')?>" rel="stylesheet">
    
    <?php elseif($controlador=='Calendario'):?>
    <link href="<?=base_url('assets/css/plugins/fullcalendar/fullcalendar.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/plugins/fullcalendar/fullcalendar.print.css')?>" rel='stylesheet' media='print'>
    <?php endif;?>
    
    <link href="<?=base_url('assets/css/animate.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">
    
    <style>
        @media print {
            .btn-group{
                display: none;
                visibility: hidden;
            }
            nav{
                display: none;
                visibility: hidden
            }
        }
    </style>

</head>

<body >
<noscript>
    <style type="text/css">
        #wrapper{display: none;}
        body {background-color: #f3f3f4;}
    </style>
    <div class="middle-box text-center animated fadeInDown">
        <h1 style="margin-bottom: 20px; font-size: 160px;">Opps!</h1>
        <h3 class="font-bold">Hemos detectado un error</h3>

        <div class="error-desc">
            Hemos detectado que su navegador tiene deshabilitado Javascript.
            Por favor <strong>habilite javascript</strong> en su navegador para poder hacer uso de CHAP.
            <div class="row">
                Para ver las instrucciones para habilitar javascript en su navegador, haga click en el boton <strong>Instrucciones</strong>,
                luego regresa a esta pagina y presiona el boton <strong>Inicio</strong>.<br>
                <a href="<?=site_url('home')?>" class="btn btn-success btn-lg"><i class="fa fa-home"></i> Inicio</a>
                <a href="http://www.enable-javascript.com/es/" class="btn btn-success btn-lg" target="_blank"><i class="fa fa-file"></i> Instrucciones</a>
            </div>
        </div>
    </div>
</noscript>
    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="<?=base_url('assets/img/2.png')?>" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?=$this->session->userdata('usuario')['usuario']?></strong>
                             </span> <span class="text-muted text-xs block"><?=$this->session->userdata('usuario')['cargo']?> <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <!--<li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>
                            <li class="divider"></li>-->
                            <li><a href="<?=site_url('login/logout')?>">Cerrar sesión</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        CHAP
                    </div>
                </li>
                <li <?=($titulo)=='CHAP - Mukumbarí STM' ? 'class="active"' : '' ?>>
                    <a href="<?=site_url('home')?>"><i class="fa fa-home "></i> <span class="nav-label">Inicio</span> </a>
                    <!--<ul class="nav nav-second-level">
                        <li ><a href="index-2.html">Dashboard v.1</a></li>
                        <li ><a href="dashboard_2.html">Dashboard v.2</a></li>
                        <li ><a href="dashboard_3.html">Dashboard v.3</a></li>
                        <li ><a href="dashboard_4_1.html">Dashboard v.4</a></li>
                    </ul>-->
                </li>
                <?php if($this->session->userdata('usuario')['rol']==1):/// aqui restriccion ?>
                <li <?=($titulo)=='CHAP - Empleados' ? 'class="active"' : '' ?>>
                    <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Empleados</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li ><a href="<?=site_url('empleado/index')?>"><i class="fa fa-bars"></i> Listar</a></li>
                        <li ><a href="<?=site_url('empleado/insertar')?>"><i class="fa fa-plus" ></i> Nuevo</a></li>
                    </ul>
                </li>
                <li <?=($titulo)=='CHAP - Asignacion' ? 'class="active"' : '' ?>>
                    <a href="<?=site_url('asginacion')?>"><i class="fa fa-calendar-o"></i> <span class="nav-label">Asignación<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?=site_url('asignacion')?>"><i class="fa fa-bars"></i> Listar</a></li>
                        <li><a href="<?=site_url('asignacion/insertar')?>"><i class="fa fa-plus"></i> Nueva</a></li>
                    </ul>
                </li>

                <li <?=($titulo)=='CHAP - Departamentos' || ($titulo)=='CHAP - Cargos' ? 'class="active"' : '' ?>>
                    <a href="#"><i class="fa fa-building"></i> <span class="nav-label">Departamento</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li ><a href="<?=site_url('departamento/index')?>"><i class="fa fa-bars"></i>Listar</a></li>
                        <li ><a href="<?=site_url('departamento/insertar')?>"><i class="fa fa-plus"></i> Nuevo</a></li>   
                        <li <?=($titulo)=='CHAP - Cargos' ? 'class="active"' : '' ?>>
                            <a href="#"><i class="fa fa-briefcase"></i>Cargos <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="<?=site_url('cargo')?>"><i class="fa fa-bars"></i>Listar</a></li>
                                <li><a href="<?=site_url('cargo/insertar')?>"><i class="fa fa-plus"></i>Nuevo</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li <?=($titulo)=='CHAP - Horario' ? 'class="active"' : '' ?>>
                    <a href="#"><i class="fa fa-clock-o"></i> <span class="nav-label">Horario</span>  <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?=site_url('horario')?>"><i class="fa fa-bars"></i>Listar</a></li>
                        <li><a href="<?=site_url('horario/insertar')?>"><i class="fa fa-plus"></i>Nuevo</a></li>
                    </ul>
                </li>
                <li <?=($titulo)=='CHAP - Justificación'? 'class="active"' : ''?>>
                    <a href="#"><i class="fa fa-file-text"></i> <span class="nav-label">Justificación</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li ><a href="<?=site_url('observacion/index')?>"><i class="fa fa-bars"></i>Listar</a></li>
                        <li ><a href="<?=site_url('observacion/insertar')?>"><i class="fa fa-plus"></i> Nuevo</a></li>
                    </ul>
                </li>
                
                <li <?=($titulo)=='CHAP - Marcaje'? 'class="active"' : ''?>>
                    <a href="#"><i class="glyphicon glyphicon-sort"></i> <span class="nav-label">Marcajes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?=site_url('marcaje/listar')?>"><i class="fa fa-bars"></i> Listar</a></li>
                            <li><a href="<?=site_url('marcaje/listar_noti')?>"><i class="fa fa-bars"></i> Retardos</a></li>
                     
                        </ul>
                </li>
                <li <?=($titulo)=='CHAP - Feriados' ? 'class="active"' : '' ?>>
                    <a href="#"><i class="fa fa-calendar"></i> <span class="nav-label">Calendario</span><span class="pull-right fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?=site_url('feriados/calendario')?>"><i class="fa fa-bars"></i> Abrir calendario</a></li>
                        <li>
                            <a href="#"><i class="fa fa-flag"></i>Feriados <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li ><a href="<?=site_url('feriados/index')?>"><i class="fa fa-bars"></i> Listar</a></li>
                                <li ><a href="<?=site_url('feriados/insertar')?>"><i class="fa fa-plus"></i> Nuevo</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li <?=($titulo)=='CHAP - Reportes' ? 'class="active"' : '' ?>>
                    <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Reportes <span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?=site_url('reporte/pre_nomina')?>">Pre-Nomina</a></li>
                        <li><a href="<?=site_url('reporte/listado')?>" target="_blank">Listado de Códigos</a></li>
                        <!--<li><a href="table_data_tables.html">Data Tables</a></li>-->
                    </ul>
                </li>
                <!--<li <?=($titulo)=='CHAP - Usuario' ? 'class="active"' : '' ?>>
                    
                    <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Usuario</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        
                       <li><a href="<?=site_url('usuario')?>"><i class="fa fa-bars"></i>Listar</a></li>
                      <li><a href="<?=site_url('usuario/insertar')?>"><i class="fa fa-plus"></i>Nuevo</a></li>
                    </ul>
                </li>-->
                <!-- Manual de Usuario LINK -->
                <li>
                    <a href="<?=base_url('manuales/usuario/ManualdeUsuario-TH.pdf')?>" target="_blank"><i class="fa fa-book"></i> <span class="nav-label"> Manual del usuario</span></a>
                </li>
                <!--<li <?=($titulo)=='CHAP - Estacion' ? 'class="active"' : '' ?>>
                    <a href="#"><i class="fa fa-building-o"></i> <span class="nav-label">Estacion</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li ><a href="<?=site_url('estacion/index')?>"><i class="fa fa-bars"></i> Listar</a></li>
                        <li ><a href="<?=site_url('estacion/insertar')?>"><i class="fa fa-plus"></i> Nuevo</a></li>
                    </ul>
                </li>-->
                
                <!--Aqui comienza el menú que podra vizualizar el coordinador o el supervisor-->
                  <?php elseif($this->session->userdata('usuario')['rol']==2):/// aqui restriccion ?>
                    <li <?=($titulo)=='CHAP - Empleados' ? 'class="active"' : '' ?>>
                    <a href="<?=site_url('empleado/index/'.$this->session->userdata('usuario')['departamento'])?>"><i class="fa fa-users"></i> <span class="nav-label">Empleados</span></a>
                    <!--<ul class="nav nav-second-level">
                        <li ><a href="<?=site_url('empleado/index/'.$this->session->userdata('usuario')['departamento'])?>"><i class="fa fa-bars"></i> Listar</a></li>                      
                    </ul>-->
                </li>
                <li <?=($titulo)=='CHAP - Asignacion' ? 'class="active"' : '' ?>>
                    <a href="<?=site_url('asginacion')?>"><i class="fa fa-calendar-o"></i> <span class="nav-label">Asignación<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?=site_url('asignacion')?>"><i class="fa fa-bars"></i> Listar</a></li>
                        <li><a href="<?=site_url('asignacion/insertar')?>"><i class="fa fa-plus"></i> Nueva</a></li>
                    </ul>
                </li>
                <!--<li <?=($titulo)=='CHAP - Departamentos' || ($titulo)=='CHAP - Cargos' ? 'class="active"' : '' ?>>-->
                <!--    <a href="#"><i class="fa fa-building"></i> <span class="nav-label">Departamento</span><span class="fa arrow"></span></a>-->
                <!--    <ul class="nav nav-second-level">-->
                <!--          <li ><a href="<?=site_url('departamento/listar_por_departamento/'.$this->session->userdata('usuario')['departamento'])?>"><i class="fa fa-bars"></i>Listar por departamento</a></li>-->
                <!--    </ul>-->
                <!--</li>-->
                  <li <?=($titulo)=='CHAP - Marcaje'? 'class="active"' : ''?>>
                    <a href="<?=site_url('marcaje/listar')?>"><i class="glyphicon glyphicon-sort"></i> <span class="nav-label">Marcaje<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">

                        <li><a href="<?=site_url('marcaje/listar/'.$this->session->userdata('usuario')['departamento'])?>"><i class="fa fa-bars"></i> Listar</a></li>
                       <li ><a href="<?=site_url('marcaje/listar_noti/'.$this->session->userdata('usuario')['departamento'])?>"><i class="fa fa-bars"></i> Retardos</a></li>  
            
                    </ul>
                </li>

                </li>
                <!--<li <?=($titulo)=='CHAP - Horario' ? 'class="active"' : '' ?>>-->
                <!--    <a href="#"><i class="fa fa-clock-o"></i> <span class="nav-label">Horario</span>  <span class="fa arrow"></span></a>-->
                <!--    <ul class="nav nav-second-level">-->
                <!--        <li><a href="<?=site_url('horario')?>"><i class="fa fa-bars"></i>Listar</a></li>-->
                <!--        <li><a href="<?=site_url('horario/insertar')?>"><i class="fa fa-plus"></i>Nuevo</a></li>-->
                <!--    </ul>-->
                <!--</li>-->
                <li <?=($titulo)=='CHAP - Observación'? 'class="active"' : ''?>>
                    <a href="#"><i class="fa fa-file-text"></i> <span class="nav-label">Justificicación</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                       <li ><a href="<?=site_url('observacion/index')?>"><i class="fa fa-bars"></i>Listar</a></li>
                            <li ><a href="<?=site_url('observacion/insertar')?>"><i class="fa fa-plus"></i> Nuevo</a></li>
                    </ul>
                </li>
                <li <?=($titulo)=='CHAP - Reportes' ? 'class="active"' : '' ?>>
                    <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Reportes <span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?=site_url('reporte/pre_nomina')?>">Pre-Nomina</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?=base_url('manuales/usuario/ManualdeUsuario-CoS-D.pdf')?>" target="_blank"><i class="fa fa-book"></i> <span class="nav-label"> Manual del usuario</span></a>
                </li>
                <!--Menu para usuarios nivel 3 -->
                <?php elseif($this->session->userdata('usuario')['rol']==3):/// aqui restriccion ?>
                <li <?=($titulo)=='CHAP - Empleados' ? 'class="active"' : '' ?>>
                    <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Empleados</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li ><a href="<?=site_url('empleado')?>"><i class="fa fa-bars"></i> Listar</a></li>       
                    </ul>
                </li>
                <li <?=($titulo)=='CHAP - Usuario' ? 'class="active"' : '' ?>>
                    <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Usuario</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?=site_url('usuario')?>"><i class="fa fa-bars"></i>Listar</a></li>
                        <li><a href="<?=site_url('usuario/insertar')?>"><i class="fa fa-plus"></i>Nuevo</a></li>
                    </ul>
                </li>
                <!--<li>
                    <a href="#"><i class="fa fa-book"></i> <span class="nav-label"> Manual del usuario</span></a>
                </li>-->
                <li>
                    <a href="<?=base_url('manuales/sistema')?>"><i class="fa fa-question"></i> <span class="nav-label"> Documentación del sistema</span></a>
                </li>
                <!--<li>-->
                <!--    <a href="#"><i class="fa fa-gear"></i> <span class="nav-label"> Herramientas</span><span class="fa arrow"></span></a>-->
                <!--    <ul class="nav nav-second-level">-->
                <!--        <li><a href="#"><i class="fa fa-database"></i> Operaciones en Base de datos</a></li>-->
                <!--    </ul>-->
                <!--</li>-->
                <?php endif;?>
            </ul>
        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <!--<form role="search" class="navbar-form-custom" action="../../external.html?link=http://webapplayers.com/inspinia_admin-v2.0/search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>-->
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <!--<li>
                    <span class="m-r-sm text-muted welcome-message">CHAP - Sistema Automatizado de Control de Horarios y Asistencias de personal.</span>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <div id="prueba">
                            
                        </div>
                        <li>
                            <div class="dropdown-messages-box">
                                <div class="media-body">
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>-->
            
            
  <?php if($this->session->userdata('usuario')['rol']==1):/// aqui restriccion ?>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" title="Notificaciones">
                        <i class="fa fa-bell"></i>  <span class="label label-primary" id="id"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <div id="content" style="height: 500px;">
                            <li>
                                Sin notificaciones
                            </li>
                        </div>

                    
                 <div id="response"></div>
                        <li>
                            <div class="text-center link-block">
                                    <a href="<?=site_url('marcaje/listar_noti')?>">
                                    <strong>Ver todas</strong></a>
                                    <!--<a href="" class="disabled">Marcar todo como leido</a>-->
                            </div>
                        </li>
                    </ul>
                </li>

 <?php elseif($this->session->userdata('usuario')['rol']==2):/// aqui restriccion ?>     
 
           <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" title="Notificaciones" >
                    <i class="fa fa-bell"></i>  <span class="label label-primary" id="id1"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <div id="content1" style="width: 500px" >
                            <li>
                                Sin notificaciones
                            </li>
                        </div>
                        <li>
                            <div class="text-center link-block">
                                    <a href="<?=site_url('marcaje/listar_noti/'.$this->session->userdata('usuario')['departamento'])?>">
                                    <strong>Ver todas</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
               <?php endif;?>  
                <li>
                    <a href="<?=site_url('login/logout')?>">
                        <i class="fa fa-sign-out"></i> Cerrar sesión
                    </a>
                </li>
            </ul>

        </nav>
        </div>
