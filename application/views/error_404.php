<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/404.html by HTTraQt Website Copier/1.x [Karbofos 2010-2014] jue, 07 may 2015 11:13:02 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="apple-touch-icon" sizes="57x57"   href="<?=base_url('assets/img/favicons/apple-touch-icon-57x57.png')?>">
    <link rel="apple-touch-icon" sizes="60x60"   href="<?=base_url('assets/img/favicons/apple-touch-icon-60x60.png')?>">
    <link rel="apple-touch-icon" sizes="72x72"   href="<?=base_url('assets/img/favicons/apple-touch-icon-72x72.png')?>">
    <link rel="apple-touch-icon" sizes="76x76"   href="<?=base_url('assets/img/favicons/apple-touch-icon-76x76.png')?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/img/favicons/apple-touch-icon-114x114.png')?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url('assets/img/favicons/apple-touch-icon-120x120.png')?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url('assets/img/favicons/apple-touch-icon-144x144.png')?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url('assets/img/favicons/apple-touch-icon-152x152.png')?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('assets/img/favicons/apple-touch-icon-180x180.png')?>">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-32x32.png')?>" sizes="32x32">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-194x194.png')?>" sizes="194x194">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-96x96.png')?>" sizes="96x96">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/android-chrome-192x192.png')?>" sizes="192x192">
    <link rel="icon" type="image/png" href="<?=base_url('assets/img/favicons/favicon-16x16.png')?>" sizes="16x16">
    <link rel="manifest" href="<?=base_url('assets/img/favicons/manifest.json')?>">
    <meta name="msapplication-TileColor" content="#e0e0e0">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#c9c9c9">

    <title>CHAP - Pagina no encontrada</title>

    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet">

    <link href="<?=base_url('assets/css/animate.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1 style="margin-bottom: 20px; font-size: 160px;">Opps!</h1>
        <h3 class="font-bold">Pagina no encontrada</h3>

        <div class="error-desc">
            Lo sentimos, la pagina que estabas buscando no ha sido encontrada. Prueba revisando la URL, si la URL es correcta y aún no puedes accder a la pagina por favor comunicate con el administrador del sistema.
            <div class="row">
                <button type="button" onclick="history.go(-1)" class="btn btn-primary btn-lg"><i class="fa fa-reply"></i> Atras</button>
                <a href="<?=site_url('home')?>" class="btn btn-success btn-lg"><i class="fa fa-home"></i> Inicio</a>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.0/404.html by HTTraQt Website Copier/1.x [Karbofos 2010-2014] jue, 07 may 2015 11:13:02 GMT -->
</html>
