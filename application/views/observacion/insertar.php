  <?php
$selec_empleados = array(''=>'- Seleccione -');
    foreach($empleados as $empleado)
    {
   
        $selec_empleados[$empleado->empleado_id] = $empleado->nombre;
    }
   

?>
<!--<script type="text/javascript">-->
<!--     var phpidioma   ='<?=setlocale(LC_TIME, 'es_VE.UTF-8') ?>';-->
<!--    var phpDate      ='<?= date('Y-m-d') ?>';-->
<!--    //var jsDate       = new Date(<?= $timestamp ?>);-->
<!--    var phpDatei      ='<?= date('H:i:s',$timestamp) ?>';-->
<!--    var phpDated      ='<?= strftime("%A") ?>';-->
<!---->
<!-- var phpDatei= $phpDatei;-->
<!---->
<!--</script>-->
<?php 

$datosf = array(
              'fecha'  => date('Y-m-d'),
        
            );

 

?>

<?php
$tipo = array(                       
                        ''         => '- Seleccione -',
                       'Falta Justificada'      => 'Falta Justificada',
                       'Falta Injustificada'    => 'Falta Injustificada',
                       'Observación'            => 'Observación',
                       'Salida antes de tiempo' => 'Salida antes de tiempo');
?>

<?php
$tipo2 = array(                       
                       ''         => '- Seleccione -',
                       'Si'=> 'Si',
                       'No'=> 'No');

?>

  <div class="wrapper wrapper-content">
       
  <div class="row">
     <div class="col-lg-10 col-lg-offset-1 animated fadeInRight">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <a href="<?=site_url('home')?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Cancelar</a>
                </div>
                <h2>
                    Redactar justificación
                </h2>
                <?=validation_errors()?>
            </div>
                <div class="mail-box">
                            <div class="mail-body">

                                <form class="form-horizontal" method="post">
                                    <div class="form-group"><label class="col-sm-2 control-label">Dirigido a:</label>

                                        <div class="col-sm-10"><input type="text" class="form-control" readonly="readonly" value="Mukumbari.Teleferico de Mérida"></div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Realizado por:</label>

                                        <div class="col-sm-10"><input type="text" class="form-control" value="<?=$usuario; ?>" readonly="readonly"></div>
                                    </div>
                                    </form>
                                <div class="mail-text h-200">

                                   <div class="form-group">
                                        <h3>Descripción del Jutificativo</h3>
                                        <?php
                                        $url = uri_string();
                                            
                                        if ($url=='observacion/insertar' || $url=='observacion/insertar/2' || $url=='observacion/insertar/1') 
                                        echo form_open('observacion/insertar/2', array('role'=>'form','id'=>'form','autocomplete'=>'off'));
                                        else
                                        echo form_open('observacion/insertar_usu/'.$empleados[0]->empleado_id.'/2', array('role'=>'form','id'=>'form','autocomplete'=>'off'));
                                        ?>
                                        <?=form_textarea(array('id'=>'descripcion','name'=>'descripcion','class'=>'form-control','value'=>set_value('descripcion'),'required'=>'required'))?>
                                        <!--<div class="summernote">
                                            <div class="click2edit wrapper p-md">
                                                
                                            </div>
                                            <textarea hidden name="descripcion" id="hola"></textarea>
                                          </div>
                                        <button id="edit" class="btn btn-primary btn-xs"  type="button">Crear</button>
                                        <button id="save" class="btn btn-primary  btn-xs" type="button">Salvar</button>-->
                                    </div>
                                     <div class="form-group">
                                            <?=form_label('Tipo:','tipo')?>
                                          <?=form_dropdown('tipo',$tipo, set_value('tipo'),'class="form-control" required="required"')?>
                                        
                                     </div>
                                     <div class="form-group">
                                          <?=form_label('¿Asistio?:','asistencia')?>
                                         <?=form_dropdown('asistencia',$tipo2, set_value('tipo'),'class="form-control" required="required"')?>                                            
                                      </div>
                                     <div class="form-group">
                                        <?=form_label('Empleado:','empleado_id')?>
                                        <?php
                                            //Toma URL actual y la separa para obtener solo los segmentos controlar y funcion de la misma
                                            $url = uri_string();
                                            
                                            if ($url=='observacion/insertar' || $url=='observacion/insertar/2' || $url=='observacion/insertar/1') :
                                            // Si los segmentos son los esperados imprime un select para mostrar a todos los empleados
                                        ?>
                                        <?=form_dropdown('empleado_id',$selec_empleados, set_value('empleado_id'),'class="chosen-select form-control" id="empleado" required="required"')?>

                                        <?php else
                                            // Si esto no es asi imprime un input con readonly con el nombre del empleado un input
                                            // hidden con el id de empleado
                                        :?>
                                        
                                        <?=form_input(array('name'=>'empleado','id'=>'empleado','value'=>$empleados[0]->nombre,'class'=>'form-control','readonly'=>'readonly'))?>
                                        <?=form_hidden('empleado_id',$empleados[0]->empleado_id)?>
                                        <?php endif;?>

                                        </div>
                                    </div>
                                <?=form_hidden($datosf)?>
                                  <?=form_hidden('usuario',$usuario)?>
                        
                                <div class="row">
                                          <div class="col-lg-6 col-lg-offset-3">
                                              <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                              <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                                          </div>
                                  </div>


                                          </div>
                                          
                      </div> 
                  </div>
                     <?=form_close()?>
  </div>
 </div>
     
     


   