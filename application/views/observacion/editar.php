    <?php
$selec_empleados = array(''=>'- Seleccione -');
    foreach($empleados as $empleado)
    {
        $selec_empleados[$empleado->empleado_id] = $empleado->nombre;
    }

?>
<?php
$tipo = array(                       
                        ''         => '- Seleccione -',
                       'Falta Justificada'      => 'Falta Justificada',
                       'Falta Injustificada'    => 'Falta Injustificada',
                       'Observación'            => 'Observación',
                       'Salida antes de tiempo' => 'Salida antes de tiempo');
?>

<?php
$tipo2 = array(                       
                       ''         => '- Seleccione -',
                       'Si'=> 'Si',
                       'No'=> 'No');
?>
<script type="text/javascript">
     var phpidioma   ='<?=setlocale(LC_TIME, 'es_VE.UTF-8') ?>';
    var phpDate      ='<?= date('Y-m-d') ?>';
    //var jsDate       = new Date(<?= $timestamp ?>);
    var phpDatei      ='<?= date('H:i:s',$timestamp) ?>';
    var phpDated      ='<?= strftime("%A") ?>';

 var phpDatei= $phpDatei;

</script>
<?php 

$datosf = array(
              'fecha'  => date('Y-m-d'),
        
            );

 

?>

 <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Justificación</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('observacion')?>">Justificación</a>
                        </li>
                        <li class="active">
                            <strong>Detalle de Justificación</strong>
                        </li>
                    </ol>
                </div>
          </div>
           <div class="wrapper wrapper-content">        
                <div class="row">
                   <div class="col-lg-8 col-lg-offset-2">
                        <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3>Editar Justificación</h3>
                               </div>
                               <div class="panel-body">
                                        <?=validation_errors()?>
                                     <?=form_open('observacion/editar/'.$fila[0]->codigo_justi.'/2',array('role'=>'form','id'=>'form'))?>
                                            <div class="form-group">
                                                <?=form_label('Descripcion:','descripcion')?>
                                              <?=form_textarea(array('id'=>'descripcion','class'=>'form-control','name'=>'descripcion','required'=>'required','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?>
                                            </div>
                                            <div class="form-group">
                                               <?=form_label('Tipo','tipo')?>
                                              <?=form_dropdown('tipo',$tipo,(set_value('tipo')!='' ? set_value('tipo') : $fila[0]->tipo),'class="form-control" id="tipo" required')?>
                                            </div>
                                            <div class="form-group">
                                               <?=form_label('Asistencia:','asistencia')?>
                                               <?=form_dropdown('asistencia',$tipo2,(set_value('asistencia')!='' ? set_value('asistencia') : $fila[0]->asistencia),'class="form-control" id="asistencia" required')?>
                                            </div>
                                            <div class="form-group">
                                                <?=form_label('Empleado:','empleado_id')?></td>
                                                <?=form_dropdown('empleado_id',$selec_empleados,(set_value('empleado_id')!='' ? set_value('empleado_id') : $fila[0]->empleado_id),'class="form-control" id="empleado" required')?>
                                            </div>

                                                
                                        <?=form_hidden('codigo_justi',$fila[0]->codigo_justi)?>
                                        <div class="row">
                                            <div class="col-lg-6 col-lg-offset-3">
                                                <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                                <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                                            </div>
                                        </div>
                                                
                                        <?=form_close()?>
                                </div>
                                
                               </div>   
                      <?=form_close()?>
                        </div>
                     </div>
                </div>
       