<!DOCTYPE html>
<html>


 <!--Mirrored from webapplayers.com/inspinia_admin-v2.0/video.html by HTTraQt Website Copier/1.x [Karbofos 2010-2014] jue, 07 may 2015 11:13:06 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fotografia</title>
    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/animate.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet">
</head>

   <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
   <script  src="<?=base_url('assets/js/jquery.lightbox-0.5.js')?>"></script>
   <link href="<?=base_url('assets/css/jquery.lightbox-0.5.css')?>" rel="stylesheet"/>
   <script  src="<?=base_url('assets/js/webcam.js')?>"></script>

<body  class="gray-bg">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="jumbotron">
                <h3>Espere 5 segundos mientras se toma la fotografia</h3>
                <div class="progress active">
                        <div id="pogressBar" style="width: 10%" class="progress-bar progress-bar-info">
                                <span class="sr-only" id="widthProgressBar" style="position: relative"> 10% completado</span>
                        </div>
               </div>
               <small>Mire a la camara</small>
            </div>
        </div>
         <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Camara</h5>
                              <div class="ibox-tools">
                               <!--
                               <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                              </a>-->
                                <a type="button" class="dropdown-toggle" data-toggle="dropdown" onClick="webcam.configure()">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <!--<ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>-->
                            </div>
                        <div class="ibox-content">
                             <script language="JavaScript">
                              document.write( webcam.get_html(240, 320) );//dimensiones de la camara
                              $('#pogressBar').width('50%');
                              var w ='50% completado';
                              $('#widthProgressBar').html(w)
                              </script>
                        </div>
                    </div>
                 </div> 
                </div> 
                  <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Foto Tomada</h5>
                            <div class="ibox-content">
                                <div id="upload_results" class="formulario" > </div>
                           </div>
                        </div>       
                    </div>
                </div> 
                <div class="footer">
                        <div class="pull-right">
                            <strong> UPTM Kléber Ramírez. </strong>
                        </div>
                        <div>
                            <strong>Sistema Teleférico de Mérida-Mukumbarí. </strong> 
                        </div>
                </div>
           </div>
     </div>         
  </div>

    <script language="JavaScript">
    webcam.set_hook( 'onComplete', 'my_completion_handler' );
        var w ='';    
    function do_upload() {
      // subir al servidor
      //document.getElementById('upload_results').innerHTML = '<h1>Cargando al servidor...</h1>';
        var carga = $('<h1>Cargando fotografia</h1>');
        carga.appendTo('#upload_results')
        webcam.upload();
        $('#pogressBar').width('75%');
        w ='75% completado';
        $('#widthProgressBar').html(w)
    }
    function my_completion_handler(msg) {
      //console.log(msg);
      if (msg.match(/(http\:\/\/\S+)/)) {
        var image_url = RegExp.$1;//respuesta de text.php que contiene la direccion url de la imagen
        
        // Muestra la imagen en la pantalla
        //document.getElementById('upload_results').innerHTML = 
          //'<img src="' + image_url + '">';
        $('#upload_results h1').remove();
        $('#pogressBar').width('80%');
        var img = $('<img id="fotoCargada" />');
        img.attr('src',image_url);
        img.appendTo('#upload_results');
        $('#pogressBar').width('100%');
        w ='Se completaron las acciones';
        $('#widthProgressBar').html(w)
    setTimeout(function(){
      
       top.location.href="<?=site_url('marcaje')?>";//redirection

  },12000)  
         
             
        webcam.reset();
      }
    }
  </script>

<script>


setTimeout(function(){
 
        //webcam.freeze();
        webcam.set_api_url("<?=site_url('webcam/ajax')?>");
        //webcam.set_swf_url("<?=base_url('flash/webcam.swf')?>");
        webcam.set_quality( 90 ); // calidad de la imagen
        webcam.snap();
        //do_upload();
       //webcam.set_shutter_sound( true ); // Sonido de flash
     
       },6000) //6000
        //setTimeout(function(){
        //        $.ajax({
        //        url: "<?=site_url('webcam/ajax')?>",
        //        type:'post',  
        //        success: function(respuesta){
        //        do_upload();
        //        }
        //        });
        //     },10000)   //10000


       
</script>
    <script src="<?=base_url('assets/js/plugins/metisMenu/jquery.metisMenu.js')?>"></script>
    <script src="<?=base_url('assets/js/plugins/slimscroll/jquery.slimscroll.min.js')?>"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?=base_url('assets/js/inspinia.js')?>"></script>
    <script src="<?=base_url('assets/js/plugins/pace/pace.min.js')?>"></script>
</html>

