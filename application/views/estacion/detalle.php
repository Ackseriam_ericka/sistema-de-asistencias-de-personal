<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Estacion</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('estacion')?>">Estacion</a>
                        </li>
                        <li class="active">
                            <strong>Detalle de la Estacion</strong>
                        </li>
                    </ol>
                </div>

          </div>
          
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="ibox ">
                         <?php if($fila!=0):?>
                            <div class="ibox-title">
                                <div class="m-b-md">
                                    <a href="<?=site_url('estacion/editar/'.$fila[0]->estacion_id.'/1')?>" class="btn btn-white btn-sm pull-right">Editar información</a>
                                    <h3><strong>Estacion</strong>: <?=$fila[0]->nombre?></h3>
                                </div>
                             </div>  
                             <div class="ibox-content">
                                <div class="row">
                                <div class="col-lg-5">
                                    <dl class="dl-horizontal">
                                        <dt>Altura de la Estacion:</dt> <dd><?=$fila[0]->altura?></dd>
                                        <dt>Descripcion de la Estacion:</dt><dd><?=$fila[0]->descripcion?></dd>  
                                    </dl>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <dl class="dl-horizontal">
                                            <dt>Completed:</dt>
                                            <dd>
                                                <div class="progress progress-striped active m-b-sm">
                                                    <div style="width: 60%;" class="progress-bar"></div>
                                                </div>
                                                <small>Project completed in <strong>60%</strong>. Remaining close the project, sign a contract and invoice.</small>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                           </div>
                            <?php else :?>
                                    <div class="alert alert-info">
                                        <h3>Información</h3>
                                        <span>No existen estacion registrada</span>
                                    </div>
                            <?php endif;?>
                        </div>
                    </div>
               </div>
           </div>
  
