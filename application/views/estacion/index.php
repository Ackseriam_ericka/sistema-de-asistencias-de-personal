    
      <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Estaciones</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('estacion')?>">Estaciones</a>
                        </li>
                        <li class="active">
                            <strong>Listar estaciones</strong>
                        </li>
                    </ol>
                </div>

          </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Estaciones</h3>
                    </div>
                      <div class="panel-body ">
                        <?php if($filas != 0 ):?>
                               <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th style="width:13%">Nombre de la estaciones</th>
                                        <th>Altura</th>
                                        <th>Descripción</th>
                                        <th>Acciones</th>
                                    </tr>
                              </thead>
                                <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX">
                                           <td><?=$fila->nombre?></td>
                                           <td><?=$fila->altura?></td>
                                          <td><?=$fila->descripcion?></td>
                                        <td>
                                            <div class="btn-group tooltip-demo">
                                                <a href="<?=site_url('estacion/detalle/'.$fila->estacion_id)?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Detalles"><i class="fa fa-search"></i></a>
                                                <a href="<?=site_url('estacion/editar/'.$fila->estacion_id.'/1')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Editar información"><i class="fa fa-pencil"></i></a>
                                                <!--<a href="<?=site_url('estacion/borrar/'.$fila->estacion_id)?>">Borrar</a>-->
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen estacion registrada</span>
                            </div>
                        <?php endif;?>
                    </div>
                    <div class="panel-footer">
                        <a href="<?=site_url('estacion/insertar')?>" class="btn btn-success"><i class="fa fa-plus"></i> Agregar estacion</a>
                    </div>
                </div>
            </div>
        </div>
  </div>
