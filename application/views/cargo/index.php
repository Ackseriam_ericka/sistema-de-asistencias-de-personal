 <div class="row wrapper border-bottom white-bg page-heading">   <!--migas de pan -->
                <div class="col-sm-4">
                    <h2>Cargos</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li class="active">
                            <strong>Cargos</strong>
                        </li>
                    </ol>
                </div>
               
            </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Cargos</h3>  <!--ya es la de mi tabla  -->
                    </div>
                    <div class="panel-body">
                        <?php if($filas != 0 ):?>
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th style="width: 35%">Cargo</th>
                                        <th>Descripcion</th>
                                        <th style="width: 5%">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX">
                                        <td><?=$fila->cargo?></td>
		                        <td><?=$fila->descripcion?></td>
                                        <td>
                                            <div class="btn-group tooltip-demo">
                                                <a href="<?=site_url('cargo/editar/'.$fila->cargo_id.'/1')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Editar información"><i class="fa fa-pencil"></i></a>
                                                 <!--<a href="<?=site_url('cargo/borrar/'.$fila->cargo_id)?>"class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Borrar"><i class="fa fa-times"></i></a>-->
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen Cargos registrados</span>
                            </div>
                        <?php endif;?>
                    </div>
                    <div class="panel-footer">
                        <a href="<?=site_url('cargo/insertar')?>" class="btn btn-success"><i class="fa fa-plus"></i> Agregar cargos</a>
                    </div>
                </div>
            </div>
        </div>
            </div>