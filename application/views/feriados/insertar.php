<?php

$S_dias = array(
  ''  => '- Dia -',
  '1' => '1',
  '2' => '2',
  '3' => '3',
  '4' => '4',
  '5' => '5',
  '6' => '6',
  '7' => '7',
  '8' => '8',
  '9' => '9',
  '10' => '10',
  '11' => '11',
  '12' => '12',
  '13' => '13',
  '14' => '14',
  '15' => '15',
  '16' => '16',
  '17' => '17',
  '18' => '18',
  '19' => '19',
  '20' => '20',
  '21' => '21',
  '22' => '22',
  '23' => '23',
  '24' => '24',
  '25' => '25',
  '26' => '26',
  '27' => '27',
  '28' => '28',
  '29' => '29',
  '30'  => '30',
  '31' => '31'
);
$S_meses = array(
  ''  => '- Mes -',
  '01' => 'Enero',
  '02' => 'Febrero',
  '03' => 'Marzo',
  '04' => 'Abril',
  '05' => 'Mayo',
  '06' => 'Junio',
  '07' => 'Julio',
  '08' => 'Agosto',
  '09' => 'Septiembre',
  '10' => 'Octubre',
  '11' => 'Noviembre',
  '12' => 'Diciembre'  
);
$ano = date('Y');
$S_ano = array(
  ''  => '- Año -',
  '0' => 'Fijo',
  $ano => $ano,
  $ano+1 => $ano+1,
);

?>
      <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Feriados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('feriados')?>">Feriados</a>
                        </li>
                        <li class="active">
                            <strong>Insertar estacion</strong>
                        </li>
                    </ol>
                </div>

          </div>
               <div class="wrapper wrapper-content">
                
                <div class="row">
                   <div class="col-lg-8 col-lg-offset-2">
                        <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3>Insertar Feriado</h3>
                               </div>

                                <div class="panel-body">
 
                                        <?=validation_errors()?>
                                        <?=form_open('feriados/insertar/2', array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>Fecha:</label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <?=form_dropdown('dia',$S_dias,set_value('dia'),'class="form-control" id="dia" required="required"')?>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <?=form_dropdown('mes',$S_meses,set_value('mes'),'class=" form-control" id="dia" required="required"')?>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <?=form_dropdown('ano',$S_ano,set_value('ano'),'class=" form-control" id="dia" required="required"')?>
                                                </div>
                                                 <div class="form-group">
                                                    <?=form_label('Descripcion:','descripcion')?>
                                                    <?=form_textarea(array('id'=>'descripcion','name'=>'descripcion','class'=>'form-control','value'=>set_value('descripcion'), 'required'=>'required'))?>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 col-lg-offset-3">
                                                        <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                                        <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                                                    </div>
                                                </div>
                                        <?=form_close()?>
                                </div>
                        </div>
                    </div>
             </div>
