   
      <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Feriados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('feriados')?>">Feriados</a>
                        </li>
                        <li class="active">
                            <strong>Listar dias Feriados</strong>
                        </li>
                    </ol>
                </div>

          </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Feriados</h3>
                    </div>
                      <div class="panel-body ">
                        <?php if($filas != 0 ):?>
                               <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Dia</th>
                                        <th>Mes</th>
                                        <th>Año</th>
                                        <th>Descripción</th>
                                        <th>Acciones</th>
                                    </tr>
                              </thead>
                                <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX">
                                           <td><?=$fila->dia?></td>
                                           <td><?=$fila->mes?></td>
                                            <td><?=$fila->ano?></td>
                                          <td><?=$fila->descripcion?></td>
                                        <td>
                                             <div class="btn-group tooltip-demo">
											  <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal<?=$fila->feriado_id?>"><i class="fa fa-trash"></i></button>	
                                                <a href="<?=site_url('feriados/editar/'.$fila->feriado_id.'/1')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Editar información"><i class="fa fa-pencil"></i></a>
                                          
												   
												   <!--<a href="<?=site_url('feriados/borrar/'.$fila->feriado_id)?>">Borrar</a>-->
                                            </div>
											
											
                                        </td>
                                    </tr>
									
									 <div class="modal inmodal" id="myModal<?=$fila->feriado_id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated flipInY">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-warning modal-icon"></i>
                                            <h4 class="modal-title">¡Cuidado!</h4>
                                            <small class="font-bold">Lee con atención</small>
                                        </div>
                                        <div class="modal-body">
                                            <p>Estas a punto de eliminar un día feriado del sistema para al hacer esto puedes eliminar datos importantes.</p>
                                            <p>Si esta realmente seguro que desea eliminar este día feriado del sistema presione el boton eliminar de los contrario presione
                                             el boton cancelar.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <a href="<?=site_url('feriados/borrar/'.$fila->feriado_id)?>" class="btn btn-primary">Eliminar</a>
                                        </div>
                                    </div>	
                                </div>	
                            </div>
                                    <?php endforeach;?>
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen Días Feriados registrados</span>
                            </div>
                        <?php endif;?>
                    </div>
                    <div class="panel-footer">
                        <a href="<?=site_url('feriados/insertar')?>" class="btn btn-success"><i class="fa fa-plus"></i> Agregar Dia Feriado</a>
                    </div>
                </div>
            </div>
        </div>
  </div>
