            <div class="row wrapper border-bottom white-bg page-heading">   <!--migas de pan -->
                <div class="col-sm-4">
                    <h2>Asignaciones</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li class="active">
                            <strong>Asignaciones</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Ultimas asignaciones registradas</h3>
                    </div>
                    <div class="panel-body">
                        <?php if($filas != 0 ):?>
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th style="width:10%">Fecha</th>
                                        <!--<th>Día</th>
                                        <th>Turno</th>-->
                                        <th>Empleado</th>
                                        <th>Horario</th>
                                        <th style="width: 10%">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX" >
                                        <td><?=$fila->fecha?></td>
                                        <!--<td><?=$fila->dia?></td>
                                        <td><?=$fila->turno?></td>-->
                                        <td><?=$fila->empleado?></td>
                                        <td><?=$fila->nombre?></td>
                                        <td>
                                            <div class="btn-group tooltip-demo">
                                                <!--<a href="<?=site_url('asignacion/detalle/'.$fila->asignacion_id)?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Detalles"><i class="fa fa-search"></i></a>-->
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal<?=$fila->asignacion_id?>"><i class="fa fa-trash"></i></button>
                                                <!--<a href="<?=site_url('empleado/editar/'.$fila->asignacion_id.'/1')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Editar información"><i class="fa fa-pencil"></i></a>-->
                                                <!--<a href="<?=site_url('empleado/borrar/'.$fila->empleado_id)?>">Borrar</a>-->
                                            </div>
                                        </td>
                                    </tr>
                                    <div class="modal inmodal" id="myModal<?=$fila->asignacion_id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated flipInY">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-warning modal-icon"></i>
                                            <h4 class="modal-title">¡Cuidado!</h4>
                                            <small class="font-bold">Lee con atención</small>
                                        </div>
                                        <div class="modal-body">
                                            <p>Estas a punto de eliminar una asignación para <strong><?=$fila->empleado?></strong>, al hacer esto puedes eliminar datos importantes
                                            para el trabajor.</p>
                                            <p>Si esta realmente seguro que desea eliminar esta asignación presion el boton elimnar de los controario presione
                                             el boton cancelar.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <a href="<?=site_url('asignacion/borrar/'.$fila->asignacion_id)?>" class="btn btn-primary">Eliminar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    <?php endforeach;?>
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                            
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No se han registrado asignaciones</span>
                            </div>
                        <?php endif;?>
                    </div>
                    <div class="panel-footer">
                        <a href="<?=site_url('asignacion/insertar')?>" class="btn btn-success"><i class="fa fa-plus"></i> Hacer una asignación</a>
                    </div>
                </div>
            </div>
        </div>
            </div>