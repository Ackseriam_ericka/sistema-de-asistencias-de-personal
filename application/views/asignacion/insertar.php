<?php
$S_horarios = array(''=>'- Seleccione -');
foreach($horarios as $horario)
{
    $S_horarios[$horario->horario_id] = $horario->nombre;
};
?>

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-12">
                    <h2>Asignación</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('asignacion')?>">Ultimas asignaciones</a>
                        </li>
                        <li class="active">
                            <strong>Hacer una asignación</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                
                <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>Insertar asignación</h3>
                    </div>
                    
                    <div class="panel-body">
                    
                    <?=form_open('asignacion/insertar/2',array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                    <?=validation_errors();?>
                        <div class="form-group col-lg-6" id="data_1">
                                <?=form_label('Fecha:','fecha')?>
                                <div class="input-group date " >
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input id="fecha" name="fecha" type="text" required class="form-control" value="<?=set_value('fecha')?>">
                                </div>
                        </div>
                        <div class="form-group col-lg-6">
                            <?=form_label('Día:','dia')?></td>
                            <?=form_input(array('id'=>'dia','name'=>'dia','value'=>set_value('dia'),'class'=>'form-control','required'=>'required','readonly'=>'readonly'))?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Turno','turno')?>
                            <br>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-warning btn-outline">
                                  <input type="radio" name="turno" id="turno" autocomplete="off" value="Día"><i class="fa fa-sun-o"></i> Día
                                </label>
                                <label class="btn btn-default btn-outline">
                                  <input type="radio" name="turno" id="turno" autocomplete="off" value="Noche"><i class="fa fa-moon-o"></i> Noche
                                </label>
                                <label class="btn btn-primary btn-outline">
                                  <input type="radio" name="turno" id="turno" autocomplete="off" value="Libre"><i class="glyphicon glyphicon-sunglasses"></i> Día libre
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <?=form_label('¿Trabaja?','trabaja')?>
                            <br>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-primary btn-outline">
                                  <input type="radio" name="trabajo" id="trabaja" autocomplete="off" value="Si"><i class=""></i> Si
                                </label>
                                <label class="btn btn-danger btn-outline">
                                  <input type="radio" name="trabajo" id="trabaja" autocomplete="off" value="No"><i class=""></i> No
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <?=form_label('¿Pernocta?','pernocta')?>
                            <br>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-primary btn-outline">
                                  <input type="radio" name="pernocta" id="pernocta" autocomplete="off" value="Si"><i class=""></i> Si
                                </label>
                                <label class="btn btn-danger btn-outline">
                                  <input type="radio" name="pernocta" id="pernocta" autocomplete="off" value="No"><i class=""></i> No
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Empleado','empleado_id')?></td>
                            <?php if($empleado=='0'):?>
                            <?php
                            $S_empleados = array(''=>'- Seleccione -');
                            foreach($empleados as $empleado)
                            {
                                $S_empleados[$empleado->empleado_id] = $empleado->nombre;
                            };
                            ?>
                            <?=form_dropdown('empleado_id',$S_empleados,set_value('empleado_id'),'class="chosen-select form-control" id="empelado_id" required="required" tabindex="2"')?>
                            <?php else:?>
                            
                            <?=form_input(array('name'=>'empleado','id'=>'empleado','value'=>$empleado[0]->nombre,'class'=>'form-control','readonly'=>'readonly'))?>
                            <?=form_hidden('empleado_id',$empleado[0]->empleado_id)?>
                            <?php endif?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Horario:','horario_id')?></td>
                            <?=form_dropdown('horario_id',$S_horarios,set_value('horario_id'),'class="form-control" id="estacion" required="required"')?>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
                                <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                            </div>
                        </div>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>
            </div>