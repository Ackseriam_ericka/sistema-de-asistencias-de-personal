
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Recuperar clave</title>

    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet">

    <link href="<?=base_url('assets/css/animate.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">

</head>

<body class="gray-bg">

      <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

            </div>
            <h2>Contraseña</h2>
             
                 
                   </div>
                    
                    <div class="panel-body">
                    
                    <?=form_open('login/editar/'.$fila[0]->usuario_id.'/2',array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                    
                    <?=validation_errors();?>
                        
                       
                       
                        <div class="form-group">
                 
                    <input type="password" name="contrasena" class="form-control" placeholder="Introduzca su clave " required="required">
                </div>
               
                 <div class="form-group">
                 
                    <input type="password" name="confcontrasena" class="form-control" placeholder="Repita contraseña " required="required">
                </div>
               
                <button type="submit" class="btn btn-primary block full-width m-b">Entrar</button>
				<?=form_close()?>
     

                <?=form_hidden('empleado_id',$fila[0]->empleado_id)?>
                
                <?=form_hidden('usuario_id',$fila[0]->usuario_id)?>
                        
                        
                    
                   
 
 </form>
            <p class="m-t"> <small>SICAP Version 1 &copy; 2015</small> </p>
        </div>
    </div>
      
    <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>

</body>

    
