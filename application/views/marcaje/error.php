<?php
    $timestamp = strtotime('now');
?>
<!--<script type="text/javascript">
     var phpidioma   ='<?=setlocale(LC_TIME, 'es_VE.UTF-8') ?>';
    var phpDate      ='<?= date('Y-m-d') ?>';
    //var jsDate       = new Date(<?= $timestamp ?>);
    var phpDatei      ='<?= date('H:i:s',$timestamp) ?>';
    var phpDated      ='<?= strftime("%A") ?>';
 var phpDatei= $phpDatei;
</script>-->
<?php 
$datosf = array(
              'fecha'  => date('Y-m-d'),
        
            );

?>
<?php 
//$datosh = array(
//              'hora' => date('H:i:s'),
//            );
?>
<?php 
$dia = array(
              'dia' => ucfirst(strftime("%A")),
            );
?>
<!DOCTYPE html>
<html>


<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CHAP - Error de marcaje</title>

    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet">

    <link href="<?=base_url('assets/css/animate.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">


</head>

<body class="gray-bg">
<div class="row animated fadeInDown">
    <div class="col-lg-8 col-lg-offset-2">
        <br>
        <div class="alert alert-info">
            <div class="row">
                <div class="col-lg-1">
                    <i class="fa fa-info-circle" style="font-size: 3em"></i>
                </div>
                <div class="col-lg-11">
                <p><strong>¿Qué hacer con el error?<br></strong>
                Estas recibido una notificación por favor leela rapidamente pues solo tienes 15 segundos para hacerlo<br>
                Según lo indique la notificación debes realizar la acción que se menciona en ella. 
                </p>
                </div>
            </div>
        </div>
    </div>
</div>
   <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>

                <h6 class="logo-name" style="color: #989898" id="reloj"></h6>

            </div>
            <h3>Marcaje</h3>



             <?php if(array_key_exists("error_msg", $_SESSION)):?> <!-- Se muestra los errores del marcaje o el mensaje de Bienvenido-->
                     <div class="alert alert-danger">
                         <h3>Bienvenido</h3>
                        <?php echo $_SESSION['error_msg'];  ?>
                     
             
                     
                      <?=form_open('marcaje/insertar_falla', array('id'=> 'form'))?>

                      </div>
                               
                                 <?php $tipo = array('tipo' => $tipo, ); ?>    

                                 <?php  $id = array('id' => $id, ); ?> 

                                  <?php $descripcion = array('descripcion' => $_SESSION['error_msg'] );?>              
                                  <?php $horaex = array('horaex' => $horaex, );?>
                                  <?php  $horaex_n = array('horaex_n' => $horaex_n, ); ?>
                                  <?php $bono_noc = array('bono_noc' => $bono_noc, ); ?>
                                 <?php $status = array('status' => 'No leido', ); ?>
                                  <?php $status_supervisor = array('status_supervisor' => 'No leido', ); ?>
                                  <?=form_hidden($id)?>
                                  <?=form_hidden($horaex)?>
                                  <?=form_hidden($horaex_n)?>
                                  <?=form_hidden($bono_noc)?>
                                 <?=form_hidden($status)?>
                                 <?=form_hidden($status_supervisor)?>
                                  <?=form_hidden($descripcion)?>
                                  <?=form_hidden($tipo)?>              
                                  <?=form_hidden($datosf)?>
                                  <input type="hidden" name="hora" id="hora">
                                  <?=form_hidden($dia)?>  
                        


                        <?=form_close()?>
           
           <!--   <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>-->
             <?php endif;?>

           <?php   if(array_key_exists("error_asi", $_SESSION)):?>
                 
                <div class="alert alert-danger">
                        <h3>Bienvenido</h3>
                     <?php echo $_SESSION['error_asi'];  ?>
                    <?=form_open('marcaje/marcaje')?>
               </div>
                <!--<button type="submit" class="btn btn-primary block full-width m-b">Aceptar</button>-->
           
             <?php endif;?>
             <?php   if(array_key_exists("error_no", $_SESSION)):?>
                <div class="alert alert-info">
                  <h3>Bienvenido</h3>
                    
                    <?php echo $_SESSION['error_no'];  ?>
                    <?=form_open('marcaje/marcaje')?>

               </div>

                <!--<button type="submit" class="btn btn-primary block full-width m-b" value="Send">Aceptar</button>-->
           
             <?php endif;?>
           
      </div>
</div>  

      <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
    <script>
        function show()
        {
            var Digital=new Date();
            var hours=Digital.getHours();
            var minutes=Digital.getMinutes();
            var seconds=Digital.getSeconds();
            var dn="am";
            var hoursN = hours;
            if (hours<10)
            hoursN = '0'+hours;
            if (hours>11){
            dn="pm";
            hoursN='0'+(hours-12);
            }
            if (hours==0)
            hoursN=12;
            if (minutes<=9)
            minutes="0"+minutes;
            else
            minutes = minutes;
            if (seconds<=9)
            seconds="0"+seconds;
            else
            seconds = seconds;
            var hora = hoursN+':'+ minutes+'<span style="font-size: 70px">'+dn+'</span>';
            var horas = hours+":"+minutes+':'+seconds;
            $('#reloj').html(hora);
            $('#hora').val(horas);
            setTimeout("show()",1)
        }
        show();
        $(document).ready(function(){
            setTimeout(function(){
                $('form').submit();
                },5000)
            })
    </script>
</body>          
</html>

