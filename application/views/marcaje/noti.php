  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Notificaciones</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('marcaje')?>">Marcaje</a>
                        </li>
                        <li class="active">
                            <strong>Listar Notificaciones</strong>
                        </li>
                    </ol>
                </div>

          </div>


            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Retardos</h3>
                    </div>
                      <div class="panel-body ">
                         <?php if($filas != 0):?>
                              <div class="panel-body ">
                               <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                    
                                        <th>Hora</th>
                                        <th>Fecha</th>
                                        <th>Dia</th>
                                        <th>Tipo</th>
                                        <th >Descripción</th> 
                                        <th style="width: 5px">Acciones</th>                  
                                        
                                    </tr>
                              </thead>
                                 <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX">
                                          <td><?=$fila->hora?></td>
                                          <td><?=$fila->fecha?></td>
                                          <td><?=$fila->dia?></td>
                                          <td><?=$fila->tipo?></td>
                                          <td><?=$fila->descripcion?></td>
                                           <td>
                                            <div class="btn-group tooltip-demo">
                                                <a href="<?=site_url('observacion/insertar_usu/'.$fila->empleado_id)?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Adjuntar justificación"><i class="fa fa-paperclip"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen Notificaciones registradas en el departamento</span>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
  </div>
 