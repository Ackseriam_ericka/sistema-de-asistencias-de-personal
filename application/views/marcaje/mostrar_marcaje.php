<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Marcaje</title>

    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet">

    <link href="<?=base_url('assets/css/animate.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">

</head>
<?php

    $timestamp = strtotime('now');

?>

<script type="text/javascript">
     var phpidioma   ='<?=setlocale(LC_TIME, 'es_VE.UTF-8') ?>';
    var phpDate      ='<?= date('Y-m-d') ?>';
    //var jsDate       = new Date(<?= $timestamp ?>);
    var phpDatei      ='<?= date('H:i:s',$timestamp) ?>';
    var phpDated      ='<?= strftime("%A") ?>';
    document.write('<br/>');
    document.write('Fecha de marcaje  : ' + phpDate);
    document.write('<br/>');
 document.write(' Hora de marcaje  : ' + phpDatei);
    document.write('<br/>');
    document.write(' Dia  : ' + phpDated);
    document.write('<br/>');
 var phpDatei= $phpDatei;

</script>

<?php 

$fecha = array(
              'fecha'  => date('Y-m-d'),
        
            );

 

?>
<?php 


$hora = array(
              'hora' => date('H:i:s'),
            );

?>
<?php 


$dia = array(
              'dia' => strftime("%A"),
            );

?>

   

<body class="gray-bg">

      <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h6 class="logo-name"><?= date('H:i',$timestamp)?></h6>
            </div>


            
            <h3>Marcaje</h3>
            <p>Ingrese su codigo unico.</p>
           <?=validation_errors()?>
         <?=form_open('marcaje/horario',array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                <div class="form-group">
                   <?=form_label('Codigo unico:','codigo_empl')?> 
                  <input type="text" name="codigo_empl" class="form-control"  required="required" onKeyUp="this.value=this.value.toUpperCase();">
                </div>
          
            
        <?=form_hidden($fecha)?>
      <?=form_hidden($hora)?>
      <?=form_hidden($dia)?>
          <button type="submit" class="btn btn-primary block full-width m-b">Guardar</button>
        <?=form_close()?>
     

  
       
            <p class="m-t"> <small>SICAP Version 1 &copy; 2015</small> </p>
        
       
      
    <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
    <!-- Jquery Validate -->
    <script src="<?=base_url('assets/js/plugins/validate/jquery.validate.min.js')?>"></script>
         
           </div>
</div>  
